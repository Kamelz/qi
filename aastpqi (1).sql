-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 10:02 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aastpqi`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_files`
--

CREATE TABLE `academic_files` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_files`
--

INSERT INTO `academic_files` (`id`, `student_id`, `path`) VALUES
(1, 8, 'academicfiles/1511810154_Screenshot from 2017-11-14 15-06-33.png'),
(2, 9, 'academicfiles/1511811456_Screenshot from 2017-11-14 15-06-33.png'),
(3, 15, 'academicfiles/1511812440_Screenshot from 2017-11-14 15-06-33.png');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `certimage` varchar(255) NOT NULL,
  `studentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `certimage`, `studentid`) VALUES
(1, 'certificates/1510137837_ES-TrafficSignals.pdf', 1),
(2, 'certificates/1510139194_applicationform.pdf', 3),
(3, 'certificates/1510145316_postStdReport.pdf', 4),
(4, 'certificates/1511810047_Screenshot from 2017-11-14 15-06-33.png', 7),
(5, 'certificates/1511810154_Screenshot from 2017-11-14 15-06-33.png', 8),
(6, 'certificates/1511811456_Screenshot from 2017-11-14 15-06-33.png', 9),
(7, 'certificates/1511812232_Screenshot from 2017-11-08 13-50-27.png', 13),
(8, 'certificates/1511812440_Screenshot from 2017-11-14 15-06-33.png', 15),
(9, 'certificates/1511812580_Screenshot from 2017-11-14 16-01-51.png', 19);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `comment` varchar(700) NOT NULL DEFAULT 'no comment'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `student_id`, `comment`) VALUES
(1, 1, 'no comment'),
(2, 3, 'no comment'),
(3, 4, 'no comment'),
(4, 7, 'Irure non nisi quo facilis quae voluptas at voluptas eveniet ipsum praesentium ut'),
(5, 8, 'Excepteur aut odit enim magna non maiores molestiae voluptate est fuga Enim esse sint dignissimos doloribus sed'),
(6, 9, 'Accusantium fugiat nihil occaecat temporibus aut itaque qui nihil consequuntur et'),
(7, 13, 'In reprehenderit ullam dolores ad iusto'),
(8, 15, 'Qui vitae laboris sit quo odit ex molestiae sint excepteur accusantium consequat Eiusmod placeat atque omnis est delectus quo'),
(9, 19, 'Praesentium sit voluptates vitae dolore non et nostrum reprehenderit accusamus id molestiae deserunt id consectetur nostrum quae illum iusto ducimus');

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `complainttext` varchar(700) NOT NULL DEFAULT 'no complaints',
  `studentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `complainttext`, `studentid`) VALUES
(1, 'no complains', 1),
(2, 'no complains', 3),
(3, 'no complains', 4),
(4, 'Voluptas est proident sunt magni deleniti delectus repellendus Illo doloremque sint a rerum iure voluptatem sit', 5),
(5, 'Voluptas est proident sunt magni deleniti delectus repellendus Illo doloremque sint a rerum iure voluptatem sit', 6),
(6, 'Voluptas est proident sunt magni deleniti delectus repellendus Illo doloremque sint a rerum iure voluptatem sit', 7),
(7, 'Quo accusantium sit qui duis sint iste nisi eligendi amet blanditiis nulla beatae', 8),
(8, 'Veniam iusto ipsum ex et repellendus Quia vel perferendis', 9),
(9, 'Et debitis lorem odio est consequatur assumenda quibusdam optio illo voluptatem accusantium consectetur accusamus minim qui nisi', 10),
(10, 'Consequat Sapiente deserunt ea qui aliqua', 11),
(11, 'Et sed molestiae nulla quibusdam molestias fugiat voluptatem velit ea corporis suscipit officia doloremque vero ratione', 12),
(12, 'Et sed molestiae nulla quibusdam molestias fugiat voluptatem velit ea corporis suscipit officia doloremque vero ratione', 13),
(13, 'Deserunt sed rerum nihil officiis ipsa minus fuga Modi necessitatibus ad saepe voluptas aut similique', 15),
(14, 'Ad suscipit quidem omnis officia asperiores beatae ipsa qui occaecat dolore quo', 16),
(15, 'Ad suscipit quidem omnis officia asperiores beatae ipsa qui occaecat dolore quo', 17),
(16, 'Ad suscipit quidem omnis officia asperiores beatae ipsa qui occaecat dolore quo', 18),
(17, 'Ad suscipit quidem omnis officia asperiores beatae ipsa qui occaecat dolore quo', 19);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `coursecode` varchar(255) NOT NULL,
  `coursename` varchar(255) NOT NULL,
  `credithours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `coursecode`, `coursename`, `credithours`) VALUES
(1, 'MG911', 'Contemporary Management', 3),
(2, 'QL912', 'Quality Assurance Concepts & Systems', 3),
(3, 'MG913', 'Statistics for Management Decisions', 3),
(4, 'MG933', 'Competing in Global Environment', 3),
(5, 'QL921', 'Quality Management', 3),
(6, 'MG922', 'Management Information Systems', 3),
(7, 'QL923', 'Statistical Quality Control', 3);

-- --------------------------------------------------------

--
-- Table structure for table `extraactivity`
--

CREATE TABLE `extraactivity` (
  `id` int(11) NOT NULL,
  `eatype` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extraactivity`
--

INSERT INTO `extraactivity` (`id`, `eatype`) VALUES
(1, 'Seminar Attendance'),
(2, 'Seminar Delivery'),
(3, 'Publication'),
(4, 'Other Activity');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `trackid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `name`, `trackid`) VALUES
(1, 'Alexandria/1', 2),
(2, 'Alexandria/2', 3),
(3, 'Aswan/1', 10),
(4, 'Alexandria/3', 10),
(5, 'Aswan/1', 3),
(6, 'Alexandria/2', 1),
(7, 'Alexandria/3', 4),
(8, 'Aswan/2', 5),
(9, 'Alexandria/3', 6),
(10, 'Aswan/1', 4),
(11, 'Alexandria/1', 3),
(12, 'Alexandria/1', 4),
(13, 'Aswan/2', 10);

-- --------------------------------------------------------

--
-- Table structure for table `group_instructor`
--

CREATE TABLE `group_instructor` (
  `group_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_instructor`
--

INSERT INTO `group_instructor` (`group_id`, `instructor_id`) VALUES
(2, 1),
(1, 3),
(3, 2),
(1, 2),
(3, 3),
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `instruction`
--

CREATE TABLE `instruction` (
  `id` int(11) NOT NULL,
  `instructionstext` varchar(255) NOT NULL,
  `studentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE `instructor` (
  `id` int(11) NOT NULL,
  `instructorname` varchar(255) NOT NULL,
  `rank` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`id`, `instructorname`, `rank`, `picture`, `position`, `number`, `cv`) VALUES
(1, 'Randa farouk', 'Teacher assistant ', '', '', 0, ''),
(2, 'rania rageh', 'Lecturer', '', '', 0, ''),
(3, 'Ahmed Gala', 'Teacher assistant ', '', '', 0, ''),
(4, 'Repudiandae necessitatibus et corrupti eu aut impedit consectetur laboris cillum quae totam consequatur eligendi et cum exercitation', 'Et ex numquam placeat necessitatibus maxime quae eum eaque', 'images/1511806158_Screenshot from 2017-11-14 15-06-33.png', 'Quae corrupti velit eiusmod voluptatem expedita cum', 480, 'cv/1511806158_Screenshot from 2017-11-14 15-06-33.png');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_teaches_course`
--

CREATE TABLE `instructor_teaches_course` (
  `instructorid` int(11) NOT NULL,
  `courseid` varchar(255) NOT NULL,
  `groupid` int(11) NOT NULL,
  `enrollmentperiod` varchar(255) NOT NULL,
  `enrollmentyear` int(11) NOT NULL,
  `day` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instructor_teaches_course`
--

INSERT INTO `instructor_teaches_course` (`instructorid`, `courseid`, `groupid`, `enrollmentperiod`, `enrollmentyear`, `day`) VALUES
(2, 'QL921', 3, 'october', 2016, 'tuesday'),
(2, 'MG933', 1, 'october', 2017, 'tuesday'),
(3, 'QL921', 3, 'october', 2017, 'tuesday'),
(3, 'MG922', 3, 'february', 2017, 'wednesday'),
(1, 'MG913', 1, 'february', 2017, 'wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `pphoto` varchar(255) NOT NULL,
  `regnum` varchar(255) NOT NULL,
  `id` int(11) NOT NULL,
  `studentname` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `collegemajor` varchar(255) NOT NULL,
  `gpa` varchar(255) NOT NULL,
  `college` varchar(255) NOT NULL,
  `telephonenumber` int(9) NOT NULL,
  `mobilenumber` int(11) NOT NULL,
  `studentemail` varchar(255) NOT NULL,
  `contractingauthority` varchar(255) NOT NULL,
  `contractingauthorityinfo` text,
  `studytcity` varchar(255) NOT NULL,
  `enrollmentperiod` varchar(255) NOT NULL,
  `enrollmentyear` int(4) NOT NULL,
  `dicountstatus` varchar(255) NOT NULL,
  `percentageofdiscount` varchar(255) NOT NULL,
  `academicstatus` varchar(255) NOT NULL,
  `groupcoordinator` int(11) NOT NULL,
  `trackid` int(11) NOT NULL,
  `groupid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`pphoto`, `regnum`, `id`, `studentname`, `nationality`, `collegemajor`, `gpa`, `college`, `telephonenumber`, `mobilenumber`, `studentemail`, `contractingauthority`, `contractingauthorityinfo`, `studytcity`, `enrollmentperiod`, `enrollmentyear`, `dicountstatus`, `percentageofdiscount`, `academicstatus`, `groupcoordinator`, `trackid`, `groupid`) VALUES
('images/1510137837_pp.jpg', '16108', 1, 'Khaled Ahmed', 'Egypt', 'IS', '3.97', 'CCIT AAST', 34796581, 1325478856, 'khaled94r@yahoo.com', 'personal', 'No extra information', 'Alexandria', 'october', 2016, 'active', '73', 'regular', 0, 2, '1'),
('images/1510138010_WhatsApp Image 2017-05-31 at 2.35.05 PM.jpeg', '12478', 2, 'Mohamed Kamel', 'Egypt', 'CS', '3.4', 'CCIT AAST', 124786523, 1348657985, 'kamel@kamel.com', 'company', 'Sorcya', 'Alexandria', 'october', 2017, 'none', '0', 'regular', 0, 3, '2'),
('images/1510139194_IMG_6571.JPG', '148567', 3, 'Yasser Hanafy', 'Egypt', 'GM', '3.34', 'Alexandria University', 148657463, 1234875687, 'yasserhanafy@gmail.com', 'company', 'The company.', 'Aswan', 'october', 2016, 'active', '25', 'regular', 0, 10, '3'),
('images/1510145316_CROPPED-12064051_10205730934726667_118096480_n.jpg', '13556', 4, 'Ayman Fawzy', 'Egypt', 'IS', '3.5', 'CCIT AAST', 34267864, 1248659786, 'khaled@khaled.com', 'personal', 'No extra information', 'Alexandria', 'october', 2017, 'active', '23', 'regular', 0, 10, '4'),
('images/1511810001_Screenshot from 2017-11-14 15-06-33.png', '366', 5, 'Delilah Mercado', 'Ipsum eu quidem adipisci ex corporis velit eaque dolor', 'Error ad velit nostrud totam nostrud quis quos proident', 'Officia id qui ut dolor exercitation et non enim omnis', 'Nostrud non dolores officia doloribus corrupti non quasi nulla dicta', 44, 18, 'wazop@gmail.com', 'protocol', 'No extra information', 'Aswan', 'october', 1988, 'pending', '49', 'conversion', 0, 3, '5'),
('images/1511810028_Screenshot from 2017-11-14 15-06-33.png', '366', 6, 'Delilah Mercado', 'Ipsum eu quidem adipisci ex corporis velit eaque dolor', 'Error ad velit nostrud totam nostrud quis quos proident', 'Officia id qui ut dolor exercitation et non enim omnis', 'Nostrud non dolores officia doloribus corrupti non quasi nulla dicta', 44, 18, 'wazop@gmail.com', 'protocol', 'No extra information', 'Aswan', 'october', 1988, 'pending', '49', 'conversion', 0, 3, '5'),
('images/1511810047_Screenshot from 2017-11-14 15-06-33.png', '366', 7, 'Delilah Mercado', 'Ipsum eu quidem adipisci ex corporis velit eaque dolor', 'Error ad velit nostrud totam nostrud quis quos proident', 'Officia id qui ut dolor exercitation et non enim omnis', 'Nostrud non dolores officia doloribus corrupti non quasi nulla dicta', 44, 18, 'wazop@gmail.com', 'protocol', 'No extra information', 'Aswan', 'october', 1988, 'pending', '49', 'conversion', 0, 3, '5'),
('images/1511810154_Screenshot from 2017-11-14 15-06-33.png', '507', 8, 'Dorian Acevedo', 'Labore sunt atque doloremque quis animi commodo autem exercitationem nobis magni', 'Obcaecati molestiae delectus reprehenderit ex quis sed molestias dolor', 'Sit nobis quaerat alias dolorem reiciendis non dolor consequat Consequatur', 'Dolore voluptates officia dolores hic atque maiores et quia omnis ullamco excepturi qui iste cupiditate veritatis tenetur reprehenderit', 69, 96, 'socaxece@hotmail.com', 'union', 'asd', 'Alexandria', 'february', 1987, 'pending', '9', 'graduated', 0, 1, '6'),
('images/1511811456_Screenshot from 2017-11-14 15-06-33.png', '290', 9, 'Hoyt Ballard', 'Dolore voluptatem vitae aliquam corporis aute voluptatem dolore reiciendis non fugiat', 'Mollitia consectetur esse eum debitis quo aut dolores nisi amet dicta laudantium occaecat in', 'Rerum dolore suscipit cupiditate voluptas exercitationem occaecat', 'Sint ea et magnam quos delectus maxime non dignissimos explicabo Sunt', 28, 79, 'pumiruruzy@yahoo.com', 'company', 'zasdas', 'Alexandria', 'october', 2002, 'active', '7', 'graduated', 0, 4, '7'),
('images/1511811693_Screenshot from 2017-11-08 13-50-27.png', '921', 10, 'Rose Stewart', 'Qui est libero mollit iste commodo dolorem voluptate natus dolores eaque consequuntur dicta illum et exercitationem', 'Impedit velit adipisicing minim fuga Esse qui exercitationem labore culpa in', 'In sunt ea illo vero anim et exercitationem quas', 'Quisquam consequatur Quaerat ut quo rerum', 60, 66, 'gypocyx@hotmail.com', 'company', 'asdas', 'Aswan', 'october', 1971, 'pending', '95', 'cancelled registration', 0, 5, '8'),
('images/1511811971_Screenshot from 2017-11-08 13-50-27.png', '632', 11, 'Dorothy Solis', 'Molestiae mollitia ut ipsa nulla inventore ex et dolore', 'A vero eligendi amet facilis voluptate adipisicing ea', 'Voluptate doloremque rerum sit soluta voluptatum numquam corporis ut', 'In et architecto non quis officia quia quibusdam', 67, 65, 'rumodoji@gmail.com', 'company', 'asd', 'Alexandria', 'october', 2004, 'pending', '80', 'graduated', 0, 6, '9'),
('images/1511812153_Screenshot from 2017-11-14 16-01-51.png', '917', 12, 'Cullen Christensen', 'Aliquip saepe voluptatem Lorem impedit praesentium enim rerum asperiores ut itaque quo et harum consequatur', 'Eiusmod eius recusandae Qui ex et minus quo aut eu totam voluptate amet iste reprehenderit qui', 'Atque reprehenderit velit quo est qui veniam quia deleniti exercitation reprehenderit necessitatibus ut sed veniam do dolor quae', 'Assumenda ratione obcaecati enim dolorem aspernatur', 62, 86, 'tibewy@yahoo.com', 'union', 'asdasd', 'Aswan', 'february', 2008, 'pending', '33', 'cancelled registration', 0, 4, '10'),
('images/1511812232_Screenshot from 2017-11-14 16-01-51.png', '917', 13, 'Cullen Christensen', 'Aliquip saepe voluptatem Lorem impedit praesentium enim rerum asperiores ut itaque quo et harum consequatur', 'Eiusmod eius recusandae Qui ex et minus quo aut eu totam voluptate amet iste reprehenderit qui', 'Atque reprehenderit velit quo est qui veniam quia deleniti exercitation reprehenderit necessitatibus ut sed veniam do dolor quae', 'Assumenda ratione obcaecati enim dolorem aspernatur', 62, 86, 'tibewy@yahoo.com', 'union', 'asdasd', 'Aswan', 'february', 2008, 'pending', '33', 'cancelled registration', 0, 4, '10'),
('images/1511812369_Screenshot from 2017-11-14 15-06-33.png', '158', 14, 'Rhonda Bradley', 'Omnis quas minus aut sint sunt aut earum et voluptatem Perspiciatis ipsum dolorem', 'Nam dolore voluptatibus harum et culpa dicta aut ab et pariatur Ipsum', 'Est praesentium cillum quis rerum repudiandae quo exercitationem ea nobis consequat Saepe similique rem cupidatat eum', 'Sint qui ducimus eos tempora quo impedit asperiores id veritatis distinctio Vitae velit odio repudiandae voluptatem quis tempore', 94, 43, 'sexe@gmail.com', 'union', 'asdasd', 'Alexandria', 'october', 1991, 'pending', '55', 'cancelled registration', 0, 3, '11'),
('images/1511812440_Screenshot from 2017-11-08 13-50-27.png', '158', 15, 'Rhonda Bradley', 'Omnis quas minus aut sint sunt aut earum et voluptatem Perspiciatis ipsum dolorem', 'Nam dolore voluptatibus harum et culpa dicta aut ab et pariatur Ipsum', 'Est praesentium cillum quis rerum repudiandae quo exercitationem ea nobis consequat Saepe similique rem cupidatat eum', 'Sint qui ducimus eos tempora quo impedit asperiores id veritatis distinctio Vitae velit odio repudiandae voluptatem quis tempore', 94, 43, 'sexe@gmail.com', 'company', 'asdasd', 'Alexandria', 'october', 1991, 'pending', '55', 'graduated', 0, 4, '12'),
('images/1511812526_Screenshot from 2017-11-14 15-06-33.png', '609', 16, 'Harrison Massey', 'Labore beatae suscipit ullam impedit laborum minim harum suscipit natus sunt aliquid error voluptas voluptatem illum ut', 'Enim hic aute debitis voluptates rem dolore consequat Eum ea sint aspernatur rerum temporibus', 'Alias non ex pariatur Officia ipsa consectetur anim eos iusto', 'Repudiandae consequatur et nihil nihil quo voluptas velit consectetur cupiditate quam est quas exercitationem a quisquam facere sed non excepteur', 96, 66, 'sehivo@yahoo.com', 'company', 'asdasd', 'Aswan', 'february', 1997, 'active', '57', 'conversion', 0, 10, '13'),
('images/1511812539_Screenshot from 2017-11-14 15-06-33.png', '609', 17, 'Harrison Massey', 'Labore beatae suscipit ullam impedit laborum minim harum suscipit natus sunt aliquid error voluptas voluptatem illum ut', 'Enim hic aute debitis voluptates rem dolore consequat Eum ea sint aspernatur rerum temporibus', 'Alias non ex pariatur Officia ipsa consectetur anim eos iusto', 'Repudiandae consequatur et nihil nihil quo voluptas velit consectetur cupiditate quam est quas exercitationem a quisquam facere sed non excepteur', 96, 66, 'sehivo@yahoo.com', 'company', 'asdasd', 'Aswan', 'february', 1997, 'active', '57', 'conversion', 0, 10, '13'),
('images/1511812560_Screenshot from 2017-11-14 15-06-33.png', '609', 18, 'Harrison Massey', 'Labore beatae suscipit ullam impedit laborum minim harum suscipit natus sunt aliquid error voluptas voluptatem illum ut', 'Enim hic aute debitis voluptates rem dolore consequat Eum ea sint aspernatur rerum temporibus', 'Alias non ex pariatur Officia ipsa consectetur anim eos iusto', 'Repudiandae consequatur et nihil nihil quo voluptas velit consectetur cupiditate quam est quas exercitationem a quisquam facere sed non excepteur', 96, 66, 'sehivo@yahoo.com', 'company', 'asdasd', 'Aswan', 'february', 1997, 'active', '57', 'conversion', 0, 10, '13'),
('images/1511812580_Screenshot from 2017-11-14 15-06-33.png', '609', 19, 'Harrison Massey', 'Labore beatae suscipit ullam impedit laborum minim harum suscipit natus sunt aliquid error voluptas voluptatem illum ut', 'Enim hic aute debitis voluptates rem dolore consequat Eum ea sint aspernatur rerum temporibus', 'Alias non ex pariatur Officia ipsa consectetur anim eos iusto', 'Repudiandae consequatur et nihil nihil quo voluptas velit consectetur cupiditate quam est quas exercitationem a quisquam facere sed non excepteur', 96, 66, 'sehivo@yahoo.com', 'company', 'asdasd', 'Aswan', 'february', 1997, 'active', '57', 'conversion', 0, 10, '13');

-- --------------------------------------------------------

--
-- Table structure for table `student_attend_course`
--

CREATE TABLE `student_attend_course` (
  `studentid` int(11) NOT NULL,
  `courseid` varchar(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `enrollmentperiod` varchar(255) NOT NULL,
  `enrollmentyear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_attend_course`
--

INSERT INTO `student_attend_course` (`studentid`, `courseid`, `day`, `enrollmentperiod`, `enrollmentyear`) VALUES
(1, 'MG911', 'monday', 'october', 2016),
(1, 'QL912', 'monday', 'february', 2017),
(2, 'MG913', 'sunday', 'october', 2017),
(2, 'MG922', 'thursday', 'october', 2017),
(3, 'MG911', 'sunday', 'october', 2016),
(3, 'MG913', 'wednesday', 'february', 2017),
(4, 'QL912', 'monday', 'october', 2017),
(5, 'MG911', 'wednesday', 'february', 1973),
(6, 'MG911', 'wednesday', 'february', 1973),
(7, 'MG911', 'wednesday', 'february', 1973),
(8, 'MG911', 'thursday', 'february', 1974),
(9, 'QL912', 'sunday', 'october', 1983),
(10, 'QL921', 'thursday', 'february', 1991),
(11, 'MG911', 'friday', 'october', 1972),
(12, 'QL923', 'friday', 'february', 1990),
(13, 'QL923', 'friday', 'february', 1990),
(15, 'MG911', 'thursday', 'october', 1988),
(16, 'MG911', 'friday', 'october', 1973),
(17, 'MG911', 'friday', 'october', 1973),
(18, 'MG911', 'friday', 'october', 1973),
(19, 'MG911', 'friday', 'october', 1973);

-- --------------------------------------------------------

--
-- Table structure for table `student_exception_group_course`
--

CREATE TABLE `student_exception_group_course` (
  `studentid` int(11) NOT NULL,
  `courseid` int(11) NOT NULL,
  `exceptiongroupname/num` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_has_extraactivity`
--

CREATE TABLE `student_has_extraactivity` (
  `student_id` int(11) NOT NULL,
  `eacode` int(11) NOT NULL,
  `extraactivityinfo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_has_extraactivity`
--

INSERT INTO `student_has_extraactivity` (`student_id`, `eacode`, `extraactivityinfo`) VALUES
(1, 1, 'Playing the game.'),
(3, 1, 'The Game.'),
(4, 4, 'The play thingy.'),
(5, 3, 'khgkj'),
(6, 3, 'khgkj'),
(7, 3, 'khgkj'),
(8, 1, 'asdsad'),
(9, 2, 'asdasd'),
(10, 1, 'asdasd'),
(11, 1, 'asd'),
(12, 1, 'asdasd'),
(13, 1, 'asdasd'),
(15, 2, 'asdasd'),
(16, 3, 'asdasd'),
(17, 3, 'asdasd'),
(18, 3, 'asdasd'),
(19, 3, 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `student_payment`
--

CREATE TABLE `student_payment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paymentfulfillment` varchar(255) NOT NULL,
  `numberofhours` varchar(255) DEFAULT 'not defined',
  `enrollmentyear` int(5) NOT NULL,
  `enrollmentperiod` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_payment`
--

INSERT INTO `student_payment` (`id`, `student_id`, `paymentfulfillment`, `numberofhours`, `enrollmentyear`, `enrollmentperiod`) VALUES
(22, 19, 'initial', 'not defined', 1980, 'october'),
(23, 19, 'hourly', '3', 1980, 'february'),
(24, 19, 'initial', 'not defined', 1980, 'october'),
(25, 19, 'hourly', '12', 1980, 'october');

-- --------------------------------------------------------

--
-- Table structure for table `student_reg_course`
--

CREATE TABLE `student_reg_course` (
  `studentid` int(11) NOT NULL,
  `courseid` varchar(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `enrollmentperiod` varchar(25) NOT NULL,
  `enrollmentyear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_reg_course`
--

INSERT INTO `student_reg_course` (`studentid`, `courseid`, `day`, `enrollmentperiod`, `enrollmentyear`) VALUES
(1, 'MG911', 'saturday', 'october', 2016),
(1, 'QL912', 'sunday', 'february', 2017),
(2, 'MG911', 'monday', 'october', 2017),
(2, 'QL921', 'wednesday', 'october', 2017),
(3, 'MG913', 'saturday', 'october', 2016),
(3, 'QL912', 'monday', 'february', 2017),
(4, 'MG913', 'sunday', 'october', 2017),
(5, 'MG911', 'wednesday', 'february', 2012),
(6, 'MG911', 'wednesday', 'february', 2012),
(7, 'MG911', 'wednesday', 'february', 2012),
(8, 'MG911', 'tuesday', 'october', 1988),
(9, 'QL912', 'sunday', 'february', 2005),
(10, 'MG911', 'saturday', 'october', 2005),
(11, 'MG911', 'friday', 'february', 2012),
(12, 'MG911', 'friday', 'october', 1992),
(13, 'MG911', 'friday', 'october', 1992),
(15, 'MG911', 'friday', 'october', 1987),
(16, 'MG933', 'friday', 'february', 2006),
(17, 'MG933', 'friday', 'february', 2006),
(18, 'MG933', 'friday', 'february', 2006),
(19, 'MG933', 'friday', 'february', 2006);

-- --------------------------------------------------------

--
-- Table structure for table `track`
--

CREATE TABLE `track` (
  `id` int(11) NOT NULL,
  `tracktype_id` int(11) NOT NULL,
  `trackname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `track`
--

INSERT INTO `track` (`id`, `tracktype_id`, `trackname`) VALUES
(1, 1, 'Arabic General'),
(2, 1, 'English General'),
(3, 1, 'Intensive General'),
(4, 2, 'Arabic'),
(5, 3, 'Quality'),
(6, 3, 'H&S'),
(7, 3, 'Hours'),
(8, 3, 'Food Safety'),
(9, 3, 'Health Care'),
(10, 1, 'English Health Care'),
(11, 1, 'English Operational Health & Safety'),
(12, 1, 'English Food & Safety');

-- --------------------------------------------------------

--
-- Table structure for table `track_types`
--

CREATE TABLE `track_types` (
  `id` int(11) NOT NULL,
  `tracktype` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_types`
--

INSERT INTO `track_types` (`id`, `tracktype`) VALUES
(1, 'Masters'),
(2, 'Pre-masters'),
(3, 'Diploma');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `is_admin`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'admin@admin.com', '$2y$10$UaD0/sgdYicfBwdP6Uaaw.gpTAadSf8upqyvJKJwCt6ktwzP7kDuu', 'QMKZqrgIrsGNW7w78VX2fyzinudL4XqLBXLND9TO8FeoEr9WD9O9UqUiEDGt', NULL, NULL),
(2, 'Igor Lambert', 0, 'a@a.com', '$2y$10$PDD25YPVaEYwIA94qbYRRekSYoIFXHjHVB3xuyro8MFHKAZluOwJe', 'px39LahFohaiJsJ9B5t0fFjGuQFXWaSncVkVx1lGV78G84n8RIiMLINxxt5X', '2017-11-27 16:07:07', '2017-11-27 16:07:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_files`
--
ALTER TABLE `academic_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studentid` (`studentid`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studentid` (`studentid`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coursecode` (`coursecode`),
  ADD KEY `coursecode_2` (`coursecode`);

--
-- Indexes for table `extraactivity`
--
ALTER TABLE `extraactivity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_instructor`
--
ALTER TABLE `group_instructor`
  ADD KEY `group_id` (`group_id`),
  ADD KEY `instructor_id` (`instructor_id`);

--
-- Indexes for table `instruction`
--
ALTER TABLE `instruction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor_teaches_course`
--
ALTER TABLE `instructor_teaches_course`
  ADD KEY `instructorid` (`instructorid`),
  ADD KEY `courseid_2` (`courseid`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_has_extraactivity`
--
ALTER TABLE `student_has_extraactivity`
  ADD KEY `eacode` (`eacode`);

--
-- Indexes for table `student_payment`
--
ALTER TABLE `student_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `track`
--
ALTER TABLE `track`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `tracktype` (`tracktype_id`);

--
-- Indexes for table `track_types`
--
ALTER TABLE `track_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_files`
--
ALTER TABLE `academic_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `extraactivity`
--
ALTER TABLE `extraactivity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `instruction`
--
ALTER TABLE `instruction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `student_payment`
--
ALTER TABLE `student_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `track`
--
ALTER TABLE `track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `track_types`
--
ALTER TABLE `track_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `academic_files`
--
ALTER TABLE `academic_files`
  ADD CONSTRAINT `academic_files_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `certificates`
--
ALTER TABLE `certificates`
  ADD CONSTRAINT `certificates_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `complaints`
--
ALTER TABLE `complaints`
  ADD CONSTRAINT `complaints_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `instructor_teaches_course`
--
ALTER TABLE `instructor_teaches_course`
  ADD CONSTRAINT `instructor_teaches_course_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `courses` (`coursecode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `instructor_teaches_course_ibfk_2` FOREIGN KEY (`instructorid`) REFERENCES `instructor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_has_extraactivity`
--
ALTER TABLE `student_has_extraactivity`
  ADD CONSTRAINT `student_has_extraactivity_ibfk_1` FOREIGN KEY (`eacode`) REFERENCES `extraactivity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_payment`
--
ALTER TABLE `student_payment`
  ADD CONSTRAINT `student_payment_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `track`
--
ALTER TABLE `track`
  ADD CONSTRAINT `track_ibfk_1` FOREIGN KEY (`tracktype_id`) REFERENCES `track_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
