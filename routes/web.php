<?php

use App\Student;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' =>'auth'], function () {
    Route::group(['middleware' => App\Http\Middleware\AdminAccess::class], function () {

        Route::post('/get/instructor/group', 'InstructorController@GetGroup');

        Route::post('/add/reg/courses/{id}', 'StudentController@addRegCourse');
        Route::post('/add/attend/courses/{id}', 'StudentController@addAttendCourse');
        Route::post('/add/payment/{id}', 'StudentController@addPayment');

        Route::post('get/course/instructors', 'CourseController@CourseInstructors');

        Route::post('get/students/attends', 'CourseController@CourseAttends');
        Route::post('get/students/reg', 'CourseController@CourseReg');
        Route::post('get/course/students/attends', 'CourseController@CourseStudentsAttends');
        Route::post('get/course/students/reg', 'CourseController@CourseStudentsReg');

        Route::get('eloquent/has-one-data', 'Eloquent\HasOneController@data');

        Route::get('/instructor','InstructorController@index');
        Route::post('/instructor','InstructorController@save');

        Route::post('/get/course/info','InstructorController@GetCourseInfo');
        Route::post('/get/academic/files','StudentController@getAacademicFiles');
        Route::post('/get/student/certificates','StudentController@getStudentCertificates');

        Route::post('/instructor/add','InstructorController@AddNewInstructor');
        Route::get('/','WelcomeController@index');
        Route::post('/student','StudentController@store');
        Route::post('/get/tracks','TracksController@getTracks');

        Route::post('/get/user/courses','StudentController@getUserCourses');

        Route::post('/get/instructor/courses','InstructorController@getInstructorCourses');

        Route::post('/get/user/extraactivity','StudentController@getUserActivity');
        Route::post('/get/user/info','StudentController@getUserInfo');

    });


    Route::get('/courses/data', 'CourseController@search');

    Route::get('/student/{id}', 'StudentController@show');
    Route::get('/student', 'StudentController@index');
    Route::get('/data', 'StudentController@search');

    Route::get('/instructor/data', 'InstructorController@search');

    Route::get('/lang','LanguageController@ToggleLanguage');

});



Auth::routes();
