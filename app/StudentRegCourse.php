<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRegCourse extends Model
{
    public $timestamps = false;

    public $table = 'student_reg_course';
}
