<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracks extends Model
{
    public $timestamps = false;
    protected $table="track";

    public function Type()
    {
        return $this->belongsTo(TrackType::class,'tracktype_id','id');
    }
}
