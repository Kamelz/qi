<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    public $timestamps = false;

    public $table="instructor";
    public $fillable =[
        'instructorname',
        'id',
        'rank',
        'picture',
        'number',
        'position',
        'cv'
    ];

}
