<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicFile extends Model
{
    public $timestamps = false;
    
        protected $fillable = [
            'pat',
            'student_id',
        ];
}
