<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentPayment extends Model
{
    public $table = 'student_payment';
    public $timestamps = false;
}
