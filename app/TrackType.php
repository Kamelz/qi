<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackType extends Model
{
    public $timestamps = false;
    public function Track()
    {
        return $this->hasMany('Tracks');
    }

}
