<?php
/**
 * Created by PhpStorm.
 * User: KAMEL
 * Date: 9/11/2017
 * Time: 3:51 PM
 */
namespace App\Services;
use App\Comment;
use App\Group;
use App\Student;
use App\AcademicFile;
use App\Certificate;
use App\Complaints;
use App\Extraactivity;
use App\StudentPayment;
use App\Tracks;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class StudentService
{
    protected $student;//student model.
    
    public function __construct(){
        $student = new Student();
        $this->student = $student;

        $this->certificate = $this->CertificateFactory();

    }
    public function CertificateFactory(){
        
        return new Certificate();
    }

    public function StudentPayment($id){

       return StudentPayment::where('student_id',$id)->get();
    }

    public function UserInfo($id){
        
       $result= DB::table('students')
            // ->join('certificates','students.id','=','certificates.studentid')
            ->join('comments','students.id','=','comments.student_id')
            ->join('complaints','students.id','=','complaints.studentid')
            // ->join('academic_files','students.id','=','academic_files.student_id')
            ->where('students.id',$id)
            // ->select('certificates.certimage','students.id','comments.comment','academic_files.path','complaints.complainttext')
            ->select('students.id','comments.comment','complaints.complainttext')
            ->get();
        return json_encode($result);

    }

    public function UserActivity($id){
      
       $result= DB::table('students')
            ->join('student_has_extraactivity','students.id','=','student_has_extraactivity.student_id')
            ->join('extraactivity','extraactivity.id','=','student_has_extraactivity.eacode')
    
            ->where('students.id',$id)
            ->select('extraactivity.eatype','student_has_extraactivity.extraactivityinfo')
            ->get();
        return json_encode($result);
    }

 
    public function UserCourses($id){

       $result= DB::table('students')
        ->join('student_attend_course','students.id','=','student_attend_course.studentid')
        ->join('courses','courses.coursecode','=','student_attend_course.courseid')
  
        // ->join('instructor_teaches_course','courses.coursecode','=','instructor_teaches_course.courseid')
  
        // ->join('instructor','instructor_teaches_course.instructorid','=','instructor.id')

        ->where('students.id',$id)
        // ->select('student_attend_course.day','instructor_teaches_course.instructorid','instructor.rank','instructor.instructorname', 'courses.coursename', 'courses.credithours')
        ->select('student_attend_course.day','student_attend_course.enrollmentperiod','student_attend_course.enrollmentyear','courses.coursecode','courses.coursename', 'courses.credithours')
        ->get();

        return json_encode($result);

    }

    public function UserRegCourses($id){

       $result= DB::table('students')
        ->join('student_reg_course','students.id','=','student_reg_course.studentid')
        ->join('courses','courses.coursecode','=','student_reg_course.courseid')
  
        // ->join('instructor_teaches_course','courses.coursecode','=','instructor_teaches_course.courseid')
  
        // ->join('instructor','instructor_teaches_course.instructorid','=','instructor.id')

        ->where('students.id',$id)
        // ->select('student_attend_course.day','instructor_teaches_course.instructorid','instructor.rank','instructor.instructorname', 'courses.coursename', 'courses.credithours')
        ->select('student_reg_course.day','student_reg_course.enrollmentperiod','student_reg_course.enrollmentyear','courses.coursecode','courses.coursename', 'courses.credithours')
        ->get();

        return json_encode($result);

    }

    public function AcademicFiles($id){
       return DB::table('students')
        ->join('academic_files','students.id','=','academic_files.student_id')
        ->where('academic_files.student_id',$id)
        ->select('academic_files.path')
        ->get();
    }

    public function StudentCertificates($id){
       return DB::table('students')
        ->join('certificates','students.id','=','certificates.studentid')
        ->where('certificates.studentid',$id)
        ->select('certificates.certimage')
        ->get();
    }

    public function store($data){

        $this->student->pphoto=$this->saveImage($data);
        $this->student->regnum=$data->regn;
        $this->student->studentname=$data->name;
        $this->student->nationality=$data->nationality;
        $this->student->collegemajor=$data->major;
        $this->student->gpa=$data->gpa;
        $this->student->college=$data->college;
        $this->student->telephonenumber=intval($data->telephone);
        $this->student->studentemail=$data->email;
        $this->student->mobilenumber=intval($data->mobile);
        $this->student->contractingauthority=$data->contractingauthority;
        if(isset($data->contractingauthorityinfo)){

            $this->student->contractingauthorityinfo=$data->contractingauthorityinfo;
        }
        else{

            $this->student->contractingauthorityinfo="No extra information";
        }
        $this->student->studytcity=$data->studycity;
        $this->student->percentageofdiscount=$data->percentageofdiscount;
        $this->student->enrollmentperiod=$data->enrollmentperiod;
        $this->student->enrollmentyear=$data->enrollmentyear;
        $this->student->dicountstatus=$data->dicountstatus;
        $this->student->academicstatus=$data->academicstatus;
        $this->student->groupcoordinator=$data->instructor;
        $this->student->trackid=$data->track;

 
        $checkIfGroupExists = Group::where('name',$data->studycity.'/'.$data->trackgroup)->where('trackid',$data->track)->first();
        
        if(!count($checkIfGroupExists)){
      
            $group = new Group();
            $group->name=$data->studycity.'/'.$data->trackgroup;
            $group->trackid=$data->track;
            $group->save();

            $this->student->groupid=$group->id;
            $this->student->save();
            $id=$this->student->id;

        }else{
            $this->student->groupid=$checkIfGroupExists->id;
            $this->student->save();
            $id=$this->student->id;

        }
    

        for($i=0; $i< count($data->coursesreg);++$i){

            DB::table('student_reg_course')->insert(
                [
                'studentid' => $id, 
                'courseid' => $data->coursesreg[$i],
                'day'=>$data->daycoursesreg[$i],
                'enrollmentperiod'=>$data->regcourse_enrollmentperiod[$i],
                'enrollmentyear'=>$data->regcourse_enrollmentyear[$i]
                ]
            );

        }

        for($i=0; $i< count($data->coursesattend);++$i){

            DB::table('student_attend_course')->insert(
                [
                'studentid' => $id,
                 'courseid' => $data->coursesattend[$i],
                 'day'=>$data->daycoursesattend[$i],
                 'enrollmentperiod'=>$data->attendedcourse_enrollmentperiod[$i],
                 'enrollmentyear'=>$data->attendedcourse_enrollmentyear[$i]
                 ]
            );
        }
        for($i=0; $i<count($data->extraactivity); $i++){

        //            if (!Extraactivity::where('id', '=',$data->extraactivity[$i] )->exists()) {
        //                $query = DB::table('extraactivity')->insert( ['eatype' => $data->extraactivity[$i]]);
        //
        //
        //                if(isset($data->extraactivityinfo[$i])){
        //
        //                    DB::table('student_has_extraactivity')->insert( ['student_id' => $id,'extraactivityinfo'=>$data->extraactivityinfo[$i],'eacode'=>DB::getPdo()->lastInsertId()]);
        //
        //                }
        //                else{
        //                    DB::table('student_has_extraactivity')->insert( ['student_id' => $id,'extraactivityinfo'=>"No extra information",'eacode'=>DB::getPdo()->lastInsertId()]);
        //
        //                }
        //
        //
        //          }else{

            if(isset($data->extraactivityinfo)){

                DB::table('student_has_extraactivity')->insert( ['student_id' => $id,'extraactivityinfo'=>$data->extraactivityinfo[$i],'eacode'=>$data->extraactivity[$i]]);

            }
            else{
                DB::table('student_has_extraactivity')->insert( ['student_id' => $id,'extraactivityinfo'=>"No extra information",'eacode'=>$data->extraactivity[$i]]);

            }

        //        }

        }


        $complaints =  new Complaints();
        if(!isset($data->complains)){
            $complaints->complainttext="no complains";
        }else{

            $complaints->complainttext=$data->complains;
        }
        $complaints->studentid=$id;
        $complaints->save();

//        dd($data->paymentfulfillment,$data->payment_enrollmentyear,$data->payment_enrollmentperiod,$data->numofhours);
        for($i=0; $i<count($data->paymentfulfillment); $i++){
            $studentPayment= new StudentPayment();
            $studentPayment->paymentfulfillment = $data->paymentfulfillment[$i];
            $studentPayment->enrollmentyear = $data->payment_enrollmentyear[$i];
            $studentPayment->enrollmentperiod = $data->payment_enrollmentperiod[$i];
            $studentPayment->student_id = $id;
            if($data->paymentfulfillment[$i]=="hourly" ){
//                dd($data->numofhours[$i]);
                if($data->numofhours[$i] == "0"){
                    $studentPayment->numberofhours = "3";
                }
              else{
                  $studentPayment->numberofhours = $data->numofhours[$i];
              }
            }
            $studentPayment->save();
        }

        $comments =  new Comment();
        if(!isset($data->comments)){
            $comments->comment="no comment";
        }else{

            $comments->comment=$data->comments;
        }
        $comments->student_id=$id;
        $comments->save();

        if(count($data->academicfiles)){

             $this->saveAcademicFiles($data,$id);
        }
        $this->saveCertificates($data,$id);
        $this->saveImage($data);
    }

    public function saveImage($data){
        
        return $data->picture->storeAs('images',time().'_'.$data->picture->getClientOriginalName());

    }

    public function saveAcademicFiles($data,$id){
        $files = $data->file('academicfiles');
        
                foreach ($files as $file){
                    $academicfiles= new AcademicFile();
                    $path=$file->storeAs('academicfiles',time().'_'.$file->getClientOriginalName());
                    $academicfiles->path=$path;
                    $academicfiles->student_id=$id;
                    $academicfiles->save();
                }
    }

    public function saveCertificates($data,$id){
        $files = $data->file('certificates');

        foreach ($files as $file){
            $certificate = $this->CertificateFactory();
            $path=$file->storeAs('certificates',time().'_'.$file->getClientOriginalName());
            $certificate->certimage=$path;
            $certificate->studentid=$id;
            $certificate->save();
        }
    }

}