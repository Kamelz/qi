<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentCourses extends Model
{
    public $timestamps = false;
    public $table = 'student_attend_course';
}
