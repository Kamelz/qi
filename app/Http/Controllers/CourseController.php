<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Course;

use Yajra\DataTables\Facades\DataTables;
class CourseController extends Controller
{
    
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {

        return Datatables::of(Course::query())
        ->addColumn('instructors', function(Course $course){
            $modal = ' 

             <a onclick="initCoursesInstructorsModal('."'".$course->coursecode."'".')" class="waves-effect waves-light btn modal-trigger" href="#Courses_instructors_modal'.$course->coursecode.'">Instructors</a>     
  
            <div id="Courses_instructors_modal'.$course->coursecode.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Courses</h4>
                <div id="courses_instructors_content'.$course->coursecode.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->addColumn('studentsattends', function(Course $course){
            $modal = ' 

             <a onclick="initCoursesStudentsModal('."'".$course->coursecode."'".')" class="waves-effect waves-light btn modal-trigger" href="#Courses_students_modal'.$course->coursecode.'">Attending Students</a>     
  
            <div id="Courses_students_modal'.$course->coursecode.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Attending students</h4>
                <div id="courses_students_content'.$course->coursecode.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->addColumn('studentsreg', function(Course $course){
            $modal = ' 

             <a onclick="initCoursesStudentsRegModal('."'".$course->coursecode."'".')" class="waves-effect waves-light btn modal-trigger" href="#Courses_students_reg_modal'.$course->coursecode.'">Registered Students</a>     
  
            <div id="Courses_students_reg_modal'.$course->coursecode.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Registered students</h4>
                <div id="courses_students_reg_content'.$course->coursecode.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })

        ->rawColumns(['studentsattends','studentsreg','instructors'])
        ->make(true);
    }

    public function CourseInstructors(Request $request){

        return DB::table('instructor')
        ->join('instructor_teaches_course','instructor.id','=','instructor_teaches_course.instructorid')
        ->where('instructor_teaches_course.courseid',$request->data)
        ->select('instructor.instructorname','instructor.rank')
        ->get();
    }

    public function CourseStudentsAttends(Request $request){

        return DB::table('students')
        ->join('student_attend_course','students.id','=','student_attend_course.studentid')
        ->where('student_attend_course.courseid',$request->data)
        ->select('student_attend_course.enrollmentperiod','student_attend_course.enrollmentyear','students.studentname','students.regnum')
        ->get();
    }

    public function CourseStudentsReg(Request $request){

        return DB::table('students')
        ->join('student_reg_course','students.id','=','student_reg_course.studentid')
        ->where('student_reg_course.courseid',$request->data)
        ->select('student_reg_course.enrollmentperiod','student_reg_course.enrollmentyear','students.studentname','students.regnum')
        ->get();
    }

    public function CourseAttends(Request $request){

        return DB::table('students')
        ->join('student_attend_course','students.id','=','student_attend_course.studentid')
        ->join('courses','courses.coursecode','=','student_attend_course.courseid')
        ->where('student_attend_course.studentid',$request->data)
        ->select('student_attend_course.day','student_attend_course.enrollmentyear','student_attend_course.enrollmentperiod','courses.coursename','courses.credithours')
        ->get();
    }

    public function CourseReg(Request $request){

        return DB::table('students')
        ->join('student_reg_course','students.id','=','student_reg_course.studentid')
        ->join('courses','courses.coursecode','=','student_reg_course.courseid')
        ->where('student_reg_course.studentid',$request->data)
        ->select('student_reg_course.day','student_reg_course.enrollmentyear','student_reg_course.enrollmentperiod','courses.coursename','courses.credithours')
        ->get();
    }

}
