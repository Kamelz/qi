<?php

namespace App\Http\Controllers;

use App\TrackType;
use Illuminate\Http\Request;

use App\Course;
use App\Extraactivity;
use App\Tracks;
use App\Instructor;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    /**
     * View welcome page.
     */
    public function index(){
        $courses = Course::get();
        $extraactivity = Extraactivity::get();
        $tracktypes = TrackType::get();
        $instructor = Instructor::get();

        return view('welcome')->with([
            'courses'=>$courses,
            'extraactivity'=>$extraactivity,
            'tracktypes'=>$tracktypes,
            'instructors'=>$instructor
        ]);
    }
}
