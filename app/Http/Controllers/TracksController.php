<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tracks;
class TracksController extends Controller
{
    public function getTracks(Request $request){

        return json_encode(Tracks::where('tracktype_id',$request->data)->get());
    }

}
