<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
class LanguageController extends Controller
{
    public function ToggleLanguage(){

    	config(['app.timezone' => 'America/Chicago']);
    	
    	if (config('app.locale') == "en"){
            $language = Session::get('language','ar'); //ar will be the default language.
            App::setLocale($language);
    	}
    	else{
            $language = Session::get('language','en'); //en will be the default language.
            App::setLocale($language);
    	}

    	 

    	echo config('app.locale');
    }
}
