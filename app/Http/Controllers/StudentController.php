<?php

namespace App\Http\Controllers;

use App\Course;
use App\Student;
use App\StudentCourses;
use App\StudentPayment;
use App\StudentRegCourse;
use Illuminate\Http\Request;
use App\Services\StudentService;
use App\Http\Requests\StudentPost;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student.search');
    }

    /**
     * show user profile page.
     *
     */
    public function show(StudentService $service , $id)
    { 
        $academicFiles        = $service->AcademicFiles($id);
        $studentCertificates  = $service->StudentCertificates($id);
        $studentInfo          = $service->UserInfo($id) ;
        $studentCourses       = $service->UserCourses($id) ;
        $studentRegCourses       = $service->UserRegCourses($id) ;
        $studentActivites     = $service->UserActivity($id);
        $studentPayments     = $service->StudentPayment($id);
        $student              = Student::where('id',$id)->first()->toArray();
        $courses = Course::get();
        return view('student.profile')->with([

            'academicFiles'          => $academicFiles,
            'studentCertificates'    => $studentCertificates,
            'studentInfo'            => json_decode($studentInfo),
            'studentCourses'         => json_decode($studentCourses),
            'studentRegCourses'      => json_decode($studentRegCourses),
            'studentActivites'       => json_decode($studentActivites),
            'studentPayments'       => json_decode($studentPayments),
            'student'                => $student,
            'courses'                => $courses,
          ]);
    }

    public function addPayment(Request $request,$id){

        $messages = [
            'paymentfulfillment[].required' => 'The payment field is required.',
            'numofhours[].required' => 'The hours field is required.',
            'payment_enrollmentperiod[].required' => 'The enrollment period is field required.',
            'payment_enrollmentyear[].required' => 'The enrollment year field is required.',
        ];

        $data = $request->validate([
            'paymentfulfillment' => 'required',
            'numofhours' => 'required',
            'payment_enrollmentperiod' => 'required',
            'payment_enrollmentyear' => 'required',
        ],$messages);

        for($i=0; $i<count($data['paymentfulfillment']); $i++){
            $studentPayment= new StudentPayment();
            $studentPayment->paymentfulfillment = $data['paymentfulfillment'][$i];
            $studentPayment->enrollmentyear = $data['payment_enrollmentyear'][$i];
            $studentPayment->enrollmentperiod = $data['payment_enrollmentperiod'][$i];
            $studentPayment->student_id = $id;
            if($data['paymentfulfillment'][$i]=="hourly" ){

                if($data['numofhours'][$i] == "0"){
                    $studentPayment->numberofhours = "3";
                }
                else{
                    $studentPayment->numberofhours = $data['numofhours'][$i];
                }
            }
            $studentPayment->save();
        }

        return redirect()->back()->with('status',trans('home.success'));
    }

    public function addRegCourse(Request $request,$id){

        $messages = [
            'regcourse_enrollmentyear[].required' => 'The enrollment year field is required.',
            'regcourse_enrollmentperiod[].required' => 'The enrollment period field is required.',
            'daycoursesreg[].required' => 'The day field is required.',
            'coursesreg[].required' => 'The course field is required.',
        ];

        $data = $request->validate([
            'regcourse_enrollmentyear' => 'required',
            'regcourse_enrollmentperiod' => 'required',
            'daycoursesreg' => 'required',
            'coursesreg' => 'required',
        ],$messages);

        for($i=0; $i<count($data['coursesreg']); $i++){
            $course = new StudentRegCourse();
            $course->studentid=$id;
            $course->courseid=$data['coursesreg'][$i];
            $course->day=$data['daycoursesreg'][$i];
            $course->enrollmentperiod=$data['regcourse_enrollmentperiod'][$i];
            $course->enrollmentyear=$data['regcourse_enrollmentyear'][$i];
            $course->save();
        }
        return redirect()->back()->with('status',trans('home.success'));
    }

    public function addAttendCourse(Request $request,$id){

        $messages = [
            'attendcourse_enrollmentyear[].required' => 'The enrollment year field is required.',
            'attendedcourse_enrollmentperiod[].required' => 'The enrollment period field is required.',
            'daycoursesattend[].required' => 'The day field is required.',
            'coursesattend[].required' => 'The course field is required.',
        ];

        $data = $request->validate([
            'attendedcourse_enrollmentyear' => 'required',
            'attendedcourse_enrollmentperiod' => 'required',
            'daycoursesattend' => 'required',
            'coursesattend' => 'required',
        ],$messages);

        for($i=0; $i<count($data['coursesattend']); $i++){
            $course = new StudentCourses();
            $course->studentid=$id;
            $course->courseid=$data['coursesattend'][$i];
            $course->day=$data['daycoursesattend'][$i];
            $course->enrollmentperiod=$data['attendedcourse_enrollmentperiod'][$i];
            $course->enrollmentyear=$data['attendedcourse_enrollmentyear'][$i];
            $course->save();
        }
        return redirect()->back()->with('status',trans('home.success'));
    }


    public function getAacademicFiles(StudentService $service , Request $request)
    {
        return $service->AcademicFiles($request->data);
    }

    public function getStudentCertificates(StudentService $service ,Request $request)
    {
        return $service->StudentCertificates($request->data);
        
    }

    public function getUserCourses(StudentService $service ,Request $request){
        
        return $service->UserCourses($request->data);
    }

    public function getUserActivity(StudentService $service ,Request $request){
      
        return $service->UserActivity($request->data);
    }

    public function getUserInfo(StudentService $service ,Request $request){
        
        return $service->UserInfo($request->data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {

        return Datatables::of(Student::query())
        ->editColumn('pphoto', function(Student $student){
            return "<img class='materialboxed ppicture initialized searched-image' src='../storage/app/".$student->pphoto."'/>";
        })
        ->rawColumns(['pphoto'])
        ->editColumn('pphoto', function(Student $student){
            return "<img class='materialboxed ppicture initialized searched-image' src='../storage/app/".$student->pphoto."'/>";
        })
      ->editColumn('studentname', function(Student $student){
            return "<a href='student/".$student->id."'>".$student->studentname." </a>";
        })
        ->addColumn('courses', function(Student $student){
            $modal = ' 

             <a onclick="initModal('.$student->id.')" class="waves-effect waves-light btn modal-trigger" href="#Courses_modal'.$student->id.'">Courses</a>     
  
            <div id="Courses_modal'.$student->id.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Courses</h4>
                <div id="content'.$student->id.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->addColumn('extra', function(Student $student){
            $modal = ' 

             <a onclick="initExtraActivityModal('.$student->id.')" class="waves-effect waves-light btn modal-trigger" href="#Activity_modal'.$student->id.'">Activities</a>     
  
            <div id="Activity_modal'.$student->id.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Activities</h4>
                <div id="Activity_content'.$student->id.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->addColumn('info', function(Student $student){
            $modal = ' 

             <a onclick="initInfoModal('.$student->id.')" class="waves-effect waves-light btn modal-trigger" href="#Info_modal'.$student->id.'">Info</a>     
  
            <div id="Info_modal'.$student->id.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Info</h4>
                <div id="Info_content'.$student->id.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->rawColumns(['pphoto','courses','extra','info','studentname'])
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StudentPost|Request $request
     * @param StudentService $service
     * @return \App\Http\Response
     */
    public function store(StudentPost $request,StudentService $service)
    {
        $service->store($request);
        return redirect('/')->with('status',trans('home.success'));
    }

}
