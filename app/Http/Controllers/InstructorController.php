<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\InstructorAddRequest;
use App\Instructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\InstructorRequest;
use Yajra\DataTables\Facades\DataTables;

class InstructorController extends Controller
{
    public function index(){
        $courses = Course::get();
        $instructors = Instructor::get();
        $groups = DB::table('group')
        ->join('track','track.id','=','group.trackid')
        ->join('track_types','track_types.id','=','track.tracktype_id')
        ->select('track.trackname','group.id','group.name','track_types.tracktype')
        ->get();       
        return view('instructor')->with([
            'courses' => $courses,
            'instructors' => $instructors,
            'groups' => $groups ,

        ]);
    }  

      public function GetGroup(Request $request){
       
        $groups = DB::table('group')
        ->join('instructor_teaches_course','instructor_teaches_course.groupid','=','group.id')
        ->where('instructor_teaches_course.instructorid',$request->data)
        ->where('instructor_teaches_course.courseid',$request->course)
        ->join('track','track.id','=','group.trackid')
        ->join('track_types','track_types.id','=','track.tracktype_id')
        ->select('track.trackname','group.id','group.name','track_types.tracktype')
        ->get();       
        return json_encode($groups);
    }

    public function search(){
        return Datatables::of(Instructor::query())
        ->editColumn('picture', function(Instructor $instructor){
            if($instructor->picture){
                return "<img class='materialboxed ppicture initialized searched-image' src='../storage/app/".$instructor->picture."'/>";              
            }
            return "no photo";
         })
        ->editColumn('cv', function(Instructor $instructor){
            if($instructor->cv){
                return "<a href='../storage/app/".$instructor->cv."'>CV</a>";             
            }
            return "no data";
         })
        ->editColumn('number', function(Instructor $instructor){
            if($instructor->number){
                return $instructor->number;              
            }
            return "no data";
         })
        ->editColumn('position', function(Instructor $instructor){
            if($instructor->position){
                return $instructor->position;              
            }
            return "no data";
         })

        ->addColumn('courses', function(Instructor $instructor){
            $modal = ' 

             <a onclick="initInstructorModal('.$instructor->id.')" class="waves-effect waves-light btn modal-trigger" href="#Instructor_Courses_modal'.$instructor->id.'">Courses</a>     
  
            <div id="Instructor_Courses_modal'.$instructor->id.'" class="modal">
              <div class="modal-content  left-align">
                <h4>Courses</h4>
                <div id="instructor_content'.$instructor->id.'"></div>
              </div>
         
            </div>
            ';
            return $modal;
        })
        ->rawColumns(['picture','cv','courses'])
        ->make(true);
    }

    public function getInstructorCourses(Request $request){
        $result= DB::table('instructor')
             ->join('instructor_teaches_course','instructor.id','=','instructor_teaches_course.instructorid')
             ->join('courses','courses.coursecode','=','instructor_teaches_course.courseid')
             ->where('instructor.id',$request->data)
             ->select('courses.coursecode','instructor_teaches_course.enrollmentperiod','instructor_teaches_course.enrollmentyear','courses.coursename', 'courses.credithours')
             ->get();
         return json_encode($result);
     }

    public function save(InstructorRequest $request){


        $courses = $request->coursesattend;
        $instructorid = $request->instructor;
        $group = $request->group;
        $day = $request->day;
        $enrollmentPeriod = $request->enrollmentperiod;
        $enrollmentYear = $request->enrollmentyear;
        
    
        $checkIfRelationExists = DB::table('group_instructor')
        ->where('group_id',$group)
        ->where('instructor_id',$instructorid)
        ->count();


        if(!$checkIfRelationExists){
            DB::table('group_instructor')->insert(
                [
                    'instructor_id' => $instructorid,
                    'group_id' => $group                ]
            );
        }

        for($i=0; $i< count($courses);++$i){
            
           $CheckIfExists= DB::table('instructor_teaches_course')->select(
                ['instructorid','courseid']
            )
            ->where('instructorid',$instructorid)
            ->where('courseid',$courses[$i])
            ->where('groupid',$group)
            ->count();

            if($CheckIfExists==0){
                DB::table('instructor_teaches_course')->insert(
                    [
                    'instructorid' => $instructorid,
                    'groupid' => $group,
                     'courseid' => $courses[$i], 
                     'day' => $day[$i], 
                     'enrollmentperiod' => $enrollmentPeriod[$i], 
                     'enrollmentyear' => $enrollmentYear[$i], 
                     ]
                );
            }else{
                return redirect('/instructor')->with('status',"Data alrady exists!");
            }
            
        }
        return redirect('/instructor')->with('status',trans('home.success'));
    }

    public function AddNewInstructor(InstructorAddRequest $request){

            $instructor = new Instructor();
            $instructor->instructorname = $request->instructor;
            $instructor->rank=$request->rank;

            if(isset($request->picture))
                 $instructor->picture=$this->saveImage($request);

            if(isset($request->number))

                  $instructor->number=$request->number;

            if(isset($request->position))

                 $instructor->position=$request->position;

            if(isset($request->cv))

                 $instructor->cv=$this->saveCV($request);

            $instructor->save();

        return redirect('/instructor')->with('status',trans('home.success'));
    }

    public function saveImage($data)
    {
        return $data->picture->storeAs('images',time().'_'.$data->picture->getClientOriginalName());

    }

    public function saveCV($data)
    {
        return $data->cv->storeAs('cv',time().'_'.$data->cv->getClientOriginalName());

    }


    public function GetCourseInfo(Request $request){
  
        return DB::table('instructor')
        ->join('instructor_teaches_course','instructor.id','=','instructor_teaches_course.instructorid')
        ->where('instructor_teaches_course.courseid',$request->data)
        ->select('instructor.instructorname','instructor.rank')
        ->get();
        
    }

}
