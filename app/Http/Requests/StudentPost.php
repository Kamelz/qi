<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'required',
            'nationality' => 'required',
            'major' => 'required',
            'name' => 'required',
            'gpa' => 'required',
            'college' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'regn' => 'required',
            'contractingauthority' => 'required',
            'studycity' => 'required',
            'percentageofdiscount' => 'required',
            'enrollmentperiod' => 'required',
            'dicountstatus' => 'required',
            'academicstatus' => 'required',
            'paymentfulfillment' => 'required',
            'track' => 'required',
            'trackgroup' => 'required',
            'daycoursesattend' => 'required',
            'daycoursesreg' => 'required',
            // 'extraactivity' => 'required',


        ];
    }


//
//    /**
//     * Get the error messages for the defined validation rules.
//     *
//     * @return array
//     */
//    public function messages()
//    {
//        return [
//            'picture.required' => trans('validation.required'),
//            'nationality.required' => 'required',
//            'name.required' => 'required',
//            'major.required' => 'required',
//            'gpa.required' => 'required',
//            'college.required' => 'required',
//            'telephone.required' => 'required',
//            'email.required' => 'required',
//            'mobile.required' => 'required',
//            'regn.required' => 'required',
//            'contractingauthority.required' => 'required',
//            'studycity.required' => 'required',
//            'percentageofdiscount.required' => 'required',
//            'enrollmentperiod.required' => 'required',
//            'dicountstatus.required' => 'required',
//            'dicountstatus.required' => 'required',
//            'academicstatus.required' => 'required',
//        ];
//    }

}
