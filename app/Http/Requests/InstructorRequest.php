<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstructorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coursesattend'=>'required',
            'instructor'=>'required',
            // 'day'=>'required',
            // 'group'=>'required',
            // 'enrollmentperiod'=>'required',
            // 'enrollmentyear'=>'required',
        ];
    }
}
