<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'regnum', 
        'studentname',
        'nationality',
        'collegemajor',
        'gpa',
        'college',
        'telephonenumber',
        'mobilenumber',
        'studentemail',
        'contractingauthority',
        'studytcity',
        'enrollmentperiod',
        'enrollmentyear',
        'dicountstatus',
        'percentageofdiscount',
        'paymentfulfillment',
        'numbofhours',
        'academicstatus',
    ];


     public function courses()
     {
         return $this->belongsToMany('App\Course')
         ->as('student_attend_course')
         ->using('App\StudentCourses');
     }
     


}
