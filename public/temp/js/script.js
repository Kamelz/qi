
var checkIfOtherSelected = function(elem){

     elem.parentNode.parentNode.parentNode.childNodes[3].style.display='block';
    
}


var checkPaymetnType = function(elem){

    if(elem.value == "initial"){

        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].classList.add("disabled");
        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].style.display="none";
        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].setAttribute("disabled","true");
        elem.parentNode.parentNode.parentNode.childNodes[3].style.display="none";

    }else{
        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].classList.remove("disabled");
        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].style.display="block";
        elem.parentNode.parentNode.parentNode.childNodes[3].childNodes[1].childNodes[1].removeAttribute("disabled");
        elem.parentNode.parentNode.parentNode.childNodes[3].style.display="block";
    }
}

//     $(".paymentfulfillment").on('change',function(){
//     alert(1);
//     if($(".paymentfulfillment").val() == "hourly"){
//
//         $('#numofhours').prop("disabled", false);
//     }else{
//         $('#numofhours').prop("disabled", true);
//     }
//     $('select').material_select();
//
// });



$('.datepicker').pickadate({
    min: new Date(),
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year
    firstDay: 1
});

(function ($) {
    $(function () {

        $('.button-collapse').sideNav();

    }); // end of document ready
})(jQuery); // end of jQuery name space

function isEmpty( el ){
    return !$.trim(el.html())
}

var initModal = function (id){ 
        $('.modal').modal();
   
    if (isEmpty($('#content'+id))) {
    
        
        var list='<ul class="collection">';
            list +='<li class="collection-item"> <button class="waves-effect waves-light btn" onclick=getAttendingCourse(this,"'+id+'")>Attending courses</button><li>';
            list +='<li class="collection-item"> <button class="waves-effect waves-light btn" onclick=getRegCourseInfo(this,"'+id+'")>Registered courses</button><li>';
        list +='</ul>';
        $('#content'+id).append(list);

    }
            
}

var initCoursesInstructorsModal = function (id){
        $('.modal').modal();
        $.ajax({
            type:'POST',
            url:'get/course/instructors',
            dataType:'JSON',
            data:{data:id},
            _token:     '{{ csrf_token() }}',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success:function(data){
                var list="";
                // console.log(data);
                function isEmpty( el ){
                    return !$.trim(el.html())
                }
                if (isEmpty($('#courses_instructors_content'+id))) {
                    if(!data.length){
                         list+='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                        
                    }else{
                       
                        data.forEach(function(entry){
                            
                            list+='<ul class="collection">';
                            list +='<li class="collection-item"><p>Instructor name: <span class="bold" >'+entry.instructorname+' </span></p></li>';
                            list +='<li class="collection-item"><p>Rank: <span class="bold">'+entry.rank+'</span> </p></li>';
                            list +='</ul>';
                            
                        });  
                    }
                    $('#courses_instructors_content'+id).append(list);
                }
                
            }
        });
}

var initCoursesStudentsModal  = function (id){
        $('.modal').modal();
        $.ajax({
            type:'POST',
            url:'get/course/students/attends',
            dataType:'JSON',
            data:{data:id},
            _token:     '{{ csrf_token() }}',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success:function(data){
                var list="";
                // console.log(data);
                function isEmpty( el ){
                    return !$.trim(el.html())
                }
                if (isEmpty($('#courses_students_content'+id))) {
                    if(!data.length){
                         list+='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                      
                    }else{
                       
                        data.forEach(function(entry){
                            
                            list+='<ul class="collection">';
                            list +='<li class="collection-item"><p>Name: <span class="bold" >'+entry.studentname+' </span></p></li>';
                            list +='<li class="collection-item"><p>Reg#: <span class="bold">'+entry.regnum+'</span> </p></li>';
                            list +='<li class="collection-item"><p>Enrollment Period: <span class="bold">'+entry.enrollmentperiod+'</span> </p></li>';
                            list +='<li class="collection-item"><p>Enrollment Year: <span class="bold">'+entry.enrollmentyear+'</span> </p></li>';
                            list +='</ul>';
                            
                        });  
                    }
                    console.log(list);
                    $('#courses_students_content'+id).append(list);
                }
                
            }
        });
}

var initCoursesStudentsRegModal  = function (id){
        $('.modal').modal();
        $.ajax({
            type:'POST',
            url:'get/course/students/reg',
            dataType:'JSON',
            data:{data:id},
            _token:     '{{ csrf_token() }}',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success:function(data){
                var list="";
                function isEmpty( el ){
                    return !$.trim(el.html())
                }
                if (isEmpty($('#courses_students_reg_content'+id))) {
                    if(!data.length){
                         list+='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                        
                    }else{
                       
                        data.forEach(function(entry){
                            
                            list+='<ul class="collection">';
                            list +='<li class="collection-item"><p>Name: <span class="bold" >'+entry.studentname+' </span></p></li>';
                            list +='<li class="collection-item"><p>Reg#: <span class="bold">'+entry.regnum+'</span> </p></li>';
                             list +='<li class="collection-item"><p>Enrollment Period: <span class="bold">'+entry.enrollmentperiod+'</span> </p></li>';
                            list +='<li class="collection-item"><p>Enrollment Year: <span class="bold">'+entry.enrollmentyear+'</span> </p></li>';
                   
                            list +='</ul>';
                            
                        });  
                    }
                    $('#courses_students_reg_content'+id).append(list);
                }
                
            }
        });
}

var initInstructorModal = function (id){
        $('.modal').modal();
        $.ajax({
            type:'POST',
            url:'get/instructor/courses',
            dataType:'JSON',
            data:{data:id},
            _token:     '{{ csrf_token() }}',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success:function(data){
                // // console.log(data);
                function isEmpty( el ){
                    return !$.trim(el.html())
                }
                if (isEmpty($('#instructor_content'+id))) {
                    if(!data.length){
                        var list='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                        $('#instructor_content'+id).append(list);
                    }else{
                        console.log(data);
                        data.forEach(function(entry){
                    
                            var list='<ul class="collection">';
                            list +='<li class="collection-item"><p>Course name: <span class="bold" >'+entry.coursename+' </span></p></li>';
                            list +='<li class="collection-item"><p>Credit hours: <span class="bold">'+entry.credithours+'</span> </p></li>';
                            list +='<li class="collection-item"><p>Enrollment Period: <span class="bold">'+entry.enrollmentperiod+'</span> </p></li>';
                            list +='<li class="collection-item"><p>Enrollment Year: <span class="bold">'+entry.enrollmentyear+'</span> </p></li>';
                             list +='<li class="collection-item"> <button class="waves-effect waves-light btn" onclick=getCourseInfo(this,"'+entry.coursecode+'")>Who teaches this course</button><li>';
                             list +='<li class="collection-item"> <button class="waves-effect waves-light btn" onclick=getGroups(this,"'+entry.coursecode+'","'+id+'")>Show group</button><li>';
                            list +='</ul>';
                    
                            
                            $('#instructor_content'+id).append(list);
                    
                        });  
                    }

                }
            }
        });
}

var getCourseInfo =function(elem,code){
   
    $.ajax({
        type:'POST',
        url:'get/course/info',
        dataType:'JSON',
        data:{data:code},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            console.log(elem);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list+='<li class="collection-item">Instructor: <span class="bold" >'+entry.instructorname+'</span> (Rank: <span class="bold" >'+entry.rank+'</span>)</li>';
            });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}
var getGroups =function(elem,course,id){
   
    $.ajax({
        type:'POST',
        url:'get/instructor/group',
        dataType:'JSON',
        data:{
            data:id,
            course:course
        },
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list+='<li class="collection-item">Group: <span class="bold" > '+entry.name+'/'+ entry.trackname+'/'+entry.tracktype+'  </span></li>';
            });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}

var getAttendingCourse =function(elem,code){
   
    $.ajax({
        type:'POST',
        url:'get/students/attends',
        dataType:'JSON',
        data:{data:code},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list +='<li class="collection-item"><p>Course name: <span class="bold" >'+entry.coursename+' </span></p></li>';
                list +='<li class="collection-item"><p>Credit hours: <span class="bold">'+entry.credithours+'</span> </p></li>';
                list +='<li class="collection-item"><p>Credit Day: <span class="bold">'+entry.day+'</span> </p></li>';
                list +='<li class="collection-item"><p>Enrollment Period: <span class="bold">'+entry.enrollmentperiod+'</span> </p></li>';
                list +='<li class="collection-item"><p>Enrollment Year: <span class="bold">'+entry.enrollmentyear+'</span> </p></li>';
                list +='<br>';
           });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}

var getRegCourseInfo =function(elem,code){
   
    $.ajax({
        type:'POST',
        url:'get/students/reg',
        dataType:'JSON',
        data:{data:code},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            console.log(elem);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list +='<li class="collection-item"><p>Course name: <span class="bold" >'+entry.coursename+' </span></p></li>';
                list +='<li class="collection-item"><p>Credit hours: <span class="bold">'+entry.credithours+'</span> </p></li>';
                list +='<li class="collection-item"><p>Credit Day: <span class="bold">'+entry.day+'</span> </p></li>';
                 list +='<li class="collection-item"><p>Enrollment Period: <span class="bold">'+entry.enrollmentperiod+'</span> </p></li>';
                list +='<li class="collection-item"><p>Enrollment Year: <span class="bold">'+entry.enrollmentyear+'</span> </p></li>';
                list +='<br>';
       
           });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}

var getAcademicFiles =function(elem,id){
   
    $.ajax({
        type:'POST',
        url:'get/academic/files',
        dataType:'JSON',
        data:{data:id},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            console.log(elem);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list +='<li class="collection-item">Academic files: <a href="../storage/app/'+entry.path+'">'+entry.path+'</a></li>'    
            });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}
var getStudentCertificates =function(elem,id){
   
    $.ajax({
        type:'POST',
        url:'get/student/certificates',
        dataType:'JSON',
        data:{data:id},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            console.log(data);
            console.log(elem);
            list = '<ul class="collection">';
           
            data.forEach(function(entry){
                list +='<li class="collection-item">Student certificate: <a href="../storage/app/'+entry.certimage+'">'+entry.certimage+'</a></li>'    
            });
            list+='</ul>';
            $(elem.parentNode).append(list);
            $(elem).remove();
        }
    });
}

var initExtraActivityModal = function (id){
        $('.modal').modal();

   
            $.ajax({
                type:'POST',
                url:'get/user/extraactivity',
                dataType:'JSON',
                data:{data:id},
                _token:     '{{ csrf_token() }}',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success:function(data){
                   
                    if (isEmpty($('#Activity_content'+id))) {
                        if(!data.length){
                            var list='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                            $('#Activity_content'+id).append(list);
                        }else{       
                            var list="";
                            data.forEach(function(entry){
                               
                                list+='<ul class="collection">';
                                list +='<li class="collection-item"><p>Activity:<span class="bold" >'+entry.eatype+'</span></p></li>';
                                if(entry.extraactivityinfo) {
                                    list +='<li class="collection-item"><p>Information:<span class="bold" >'+entry.extraactivityinfo+'</span></p></li>';
                                }else{
                                    list +='<li class="collection-item"><p>Information:<p class="bold" > no information</p></p></li>';
                                    
                                }
                                    list +='</ud>';
                            
                            
                            });
                        }console.log(list);
                        $('#Activity_content'+id).append(list);
                        
                  }
                }
            });
        

}

var initInfoModal = function (id){
        $('.modal').modal();
     
        $.ajax({
            type:'POST',
            url:'get/user/info',
            dataType:'JSON',
            data:{data:id},
            _token:     '{{ csrf_token() }}',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success:function(data){
                console.log(data);
                if (isEmpty($('#Info_content'+id))) {
              
                    data.forEach(function(entry){
                    
                        var list='<ul class="collection">';
                        // list +='<li class="collection-item">Academic files: <a href="../storage/app/'+entry.path+'">'+entry.path+'</a></li>';
                        // list +='<li class="collection-item">Certificate: <a href="../storage/app/'+entry.certimage+'">'+entry.certimage+'</a></li>';
                        list +='<li class="collection-item"><p>Comment:<div class="col s6 bold" >'+entry.comment+'</div></p></li>';
                        list +='<li class="collection-item"><p>Complaint:<div class=" bold" >'+entry.complainttext+'</div></p></li>';
                        list +='<li class="collection-item"> <button  class="waves-effect waves-light btn" onclick=getAcademicFiles(this,"'+entry.id+'")>Show academic files</button><li>';
                        list +='<li class="collection-item"> <button  class="waves-effect waves-light btn" onclick=getStudentCertificates(this,"'+entry.id+'")>Show student certificates</button><li>';
                        
                        list +='</ul>';
                    
                        $('#Info_content'+id).append(list);
                
                    });
                    if(!data.length){
                        var list='<ul class="collection"> <li class="collection-item"> No Information available </li> </ul>';
                        $('#Info_content'+id).append(list);
                    }
                }
            }
        });
    

}


var getTracks = function(id){

    $.ajax({
        type:'POST',
        url:'get/tracks',
        dataType:'JSON',
        data:{data:id},
        _token:     '{{ csrf_token() }}',
        beforeSend: function (request) {
            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },
        success:function(data){
            $('#tracks').empty();
            console.log(data);
            var select ="<select id='trackselector' name='track'></select>";
            $('#tracks').append(select);
           for(i=0; i< data.length; ++i){
               var option = "<option value="+data[i].id+" >"+data[i].trackname+"</option>";
               $("#trackselector").append(option);
           }
            $("#trackselector").material_select();
        }
    });
}

$('select').material_select();

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#ppicture').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readURL(this);
});

var remove = function(elem){
    elem.parentNode.parentNode.remove();
    
}


$(document).ready(function(){

    $('.scrollspy').scrollSpy();
    $(function () {

        var dt=  $('#courses-posts-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: 'courses/data',
            "scrollX": true,
                 columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],
            columns: [
                {data: 'coursecode', name: 'coursecode'},
                {data: 'coursename', name: 'coursename'},
                {data: 'credithours', name: 'credithours'},
                {data: 'instructors', name: 'instructors',searchable:false},
                {data: 'studentsattends', name: 'studentsattends',searchable:false},
                {data: 'studentsreg', name: 'studentsreg',searchable:false},
           
            ],
            order: [[1, 'asc']]
        });
        var dt=  $('#instructor-posts-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: 'instructor/data',
            "scrollX": true,
                 columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],
            columns: [
                {data: 'picture', name: 'picture',searchable:false},
                {data: 'instructorname', name: 'instructorname'},
                {data: 'rank', name: 'rank'},
                {data: 'position', name: 'position'},
                {data: 'number', name: 'number'},
                {data: 'cv', name: 'cv'},
                {data: 'courses', name: 'courses',searchable:false},
           
            ],
            order: [[1, 'asc']]
        }); 
       var dt=  $('#user-posts-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: 'data',
            "scrollX": true,
                 columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],
            columns: [
                {data: 'pphoto', name: 'pphoto',searchable:false},
                {data: 'regnum', name: 'regnum'},
                {data: 'studentname', name: 'studentname'},
                {data: 'gpa', name: 'gpa'},
                {data: 'nationality', name: 'nationality'},
                {data: 'collegemajor', name: 'collegemajor'},
                {data: 'telephonenumber', name: 'telephonenumber'},
                {data: 'mobilenumber', name: 'mobilenumber'},
                {data: 'college', name: 'college'},
                {data: 'studentemail', name: 'studentemail'},
                {data: 'groupcoordinator', name: 'groupcoordinator',searchable:true},
                {data: 'contractingauthority', name: 'contractingauthority'},
                {data: 'contractingauthorityinfo', name: 'contractingauthorityinfo'},
                {data: 'studytcity', name: 'studytcity'},
                {data: 'enrollmentperiod', name: 'enrollmentperiod'},
                {data: 'enrollmentyear', name: 'enrollmentyear'},
                {data: 'dicountstatus', name: 'dicountstatus'},
                {data: 'percentageofdiscount', name: 'percentageofdiscount'},
                {data: 'academicstatus', name: 'academicstatus'},
                {data: 'courses', name: 'courses',searchable:false},
                {data: 'extra', name: 'extra',searchable:false},
                {data: 'info', name: 'info',searchable:false},
           
            ],
            order: [[1, 'asc']]
        });
            

    });

    // $('.chips').on('chip.add', function(e, chip){
    //     var input ="<input class='tag-"+chip.tag+"' type='hidden' name='extraactivity[]' value='"+chip.tag+"'>";
    //         $('.otheactiviy').append(input);
    //   });
    //
    //
    // $('.chips').on('chip.delete', function(e, chip){
    //     $('.tag-'+chip.tag).remove();
    //   });
    //
    //
    //  $('#modal1').click(function(){
    //     console.log('ss');
    //     $('.modal').modal();
    // });

$("#append-activity").click(function(){
        var removebutton = " <div class= 'btn red' style='margin-left: 1%;' onclick='remove(this)'> - </div> ";

        var div = document.createElement('div');
        div.className = "row col s12";
        div.innerHTML = removebutton;

        // var itm = document.getElementsByClassName("extra-activity-form")[0];
        var itm = $(".extra-activity-form");

        var cln = itm[0].cloneNode(true);
        cln.append(div);

        // document.getElementsByClassName("extraactivity-row")
         $('.extraactivity-row').append(cln);

         $('select').material_select();

});
$("#append-payment-fullfillment").click(function(){
        var removebutton = " <div class= 'btn red' style='margin-left: 1%;' onclick='remove(this)'> - </div> ";

        var div = document.createElement('div');
        div.className = "row col s12";
        div.innerHTML = removebutton;

        // var itm = document.getElementsByClassName("extra-activity-form")[0];
        var itm = $(".payment-form");

        var cln = itm[0].cloneNode(true);
        cln.append(div);

        // document.getElementsByClassName("extraactivity-row")
         $('.paymentmethod-row').append(cln);

         $('select').material_select();

});

$("#addRegDays").click(function(){
        var removebutton = " <div class= 'btn red' onclick='remove(this)'> - </div> ";
        
        var div = document.createElement('div');
        div.innerHTML = removebutton;
        
        var itm = $(".coursesreg-row");

        var cln = itm[0].cloneNode(true);
        cln.append(div);
        
         $('.coursesreg-select').append(cln);
       
         $('select').material_select();
 
})

$("#addAttendRegDays").click(function(){
        var removebutton = " <div class= 'btn red' onclick='remove(this)'> - </div> ";
        
        var div = document.createElement('div');
        div.innerHTML = removebutton;
        
        var itm = $(".coursesattend-row");

        var cln = itm[0].cloneNode(true);
        cln.append(div);
        
         $('.coursesattend-select').append(cln);
       
         $('select').material_select();
 
})

    
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        // I use element.value instead value here, value parameter was always null
        return arg != element.value;
    }, "Value must not equal arg.");


    $("#studentform").validate({
        ignore: [],
        rules: {
            name: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
                email:true
            },


            major: {
                required: true,

            },

            gpa: {
                required: true,

            },

            college: {
                required: true,
            },

            telephone: {
                required: true,

            },

            mobile: {
                required: true,

            },

            regn: {
                required: true,

            },

            contractingauthority: {
                required: true,

            },

            track: {
                required: true,

            },

            trackgroup: {
                required: true,

            },

            studycity: {
                required: true,

            },

            "coursesreg[]": {
                required: true,

            },

            daycoursesreg: {
                required: true,

            },

            "coursesattend[]": {
                required: true,
                valueNotEquals: "default",

            },

            daycoursesattend: {
                required: true,

            },


            percentageofdiscount: {
                required: true,

            },


            enrollmentperiod: {
                required: true,

            },


            enrollmentyear: {
                required: true,

            },

            regcourse_enrollmentperiod: {
                required: true,

            },
            regcourse_enrollmentyear: {
                required: true,

            },
            attendedcourse_enrollmentperiod: {
                required: true,

            },
            attendedcourse_enrollmentyear: {
                required: true,

            },


            dicountstatus: {
                required: true,

            },


            academicstatus: {
                required: true,

            },

            paymentfulfillment: {
                required: true,

            },


            "certificates[]": {
                required: true,

            },

            nationality:"required",
        },
        //For custom messages
        messages: {
            name:{
                required: "Enter a Name",
                minlength: "Enter at least 5 characters"
            },
            email: "Enter student's Email",
            major:"Enter student's Major",
            gpa:"Enter student's GPA ",
            college:"Enter student's College ",
            telephone:"Enter student's Telephone ",
            mobile:"Enter student's Mobile ",
            regn:"Enter student's Registration number ",
            contractingauthority:"Choose Contracting Authority ",
            track:"Choose student's Track",
            trackgroup:"Choose student's Group ",
            studycity:"Choose student's City ",
            "coursesreg[]":"Choose student's Registered Course ",

            "regcourse_enrollmentperiod[]":"Enter student's enrollment period",
            "regcourse_enrollmentyear[]":"Enter student's enrollment year",
            "attendedcourse_enrollmentperiod[]":"Enter student's enrollment period",
            "attendedcourse_enrollmentyear[]":"Enter student's enrollment year ",


            daycoursesreg:"Choose student's Registered Course's Day ",
            "coursesattend[]":"Choose student's Attended Course ",
            daycoursesattend:"Choose student's Attended Course's Day ",
            percentageofdiscount:"Enter student's Percentage of Discount ",
            enrollmentperiod:"Choose student's Enrollment Period ",
            enrollmentyear:"Enter student's Enrollment Year ",
            dicountstatus:"Choose student's Dicount Status ",
            academicstatus:"Enter student's Academic Status ",
            paymentfulfillment:"Choose student's Payment Fulfillment ",
            "certificates[]":"Upload student's certificates ",
            nationality:"Enter student's nationality ",
            

        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });


    $(".success_message").click(function(){
        $(".success_message").hide();
    });
    $('.chips').material_chip();
    $("#rpremasters").click(function(){

        $('#spremasters').prop("disabled", false);
        $('#smasters').prop("disabled", true);
        $('#sdiploma').prop("disabled", true);
        $('select').material_select();

    });
    $("#rmasters").click(function(){

        $('#spremasters').prop("disabled", true);
        $('#smasters').prop("disabled", false);
        $('#sdiploma').prop("disabled", true);
        $('select').material_select();
    });
    $("#rdiploma").click(function(){

        $('#spremasters').prop("disabled", true);
        $('#smasters').prop("disabled", true);
        $('#sdiploma').prop("disabled", false);
        $('select').material_select();;
    });


    $("#academicstatus").on('change',function(){
           if($("#academicstatus").val() == "postponement" || $("#academicstatus").val() == "graduated" ){
            $('#academicfiles').removeClass("hide");
            
           }
           else{
                       $("input[name='academicfiles[]']").val(null);
    
            $('#academicfiles').addClass("hide");
           }

    });


     appended = 0;

        $("#contractingauthority").on('change', function () {


            if ($("#contractingauthority").val() != "union" || $("#contractingauthority").val() != "company") {
                $("#extrainfo").remove();
                appended=0;
            }


            if(appended == 0 ) {

                if ($("#contractingauthority").val() == "union" || $("#contractingauthority").val() == "company") {
                    var elem = "<div id='extrainfo' class='input-field col s6'><input  placeholder='extra information ' type='text' name='contractingauthorityinfo'></div>"
                    $(".contractingauthority").append(elem);
                }
                $('select').material_select();

                appended =1;
            }

        });


        $('.modal').modal();
        $('#submitactivity').on('click', function() {
            var activity= $('#activity').material_chip('data');
            if(activity.length){
                for(i=0; i<activity.length;++i){
                    $("#extraactivity").append($("<option value="+activity[i].tag+">"+activity[i].tag+"</option>"));
                   ;
                }
                $("#extraactivity").material_select()

            }
        });


});


