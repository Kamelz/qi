<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'group' => 'المجموعة',
    'brand' => 'AAST',
    'success' => 'تم تسجيل البيانات بنجاح',
    'coursesattend' => 'الكورسات التى يحضرها',
    'coursesreg' => 'الكورسات المسجلة',
    'withdrawal' => 'انسحاب',
    'other' => 'اخرى',
    'extraactivity' => 'انشطة اخرى',
    'delay' => 'تأجيل',
    'regular' => 'إنتظام',
    'Conversion' => 'تحويل',
    'Cancelrestriction' => 'إلغاء قيد',

    'submit' => 'تثبيت',
    'ppicture' => 'الصورة الشخصية',
    'name' => 'الأسم',
    'nationality' => 'الجنسية',
    'major' => 'التخصص',
    'gpa' => 'التقدير',
    'college' => 'الجامعة',
    'telephone' => 'تليفون',
    'mobile' => 'موبايل',
    'email' => 'البريد الإلكترونى',
    'regn' => 'ؤقم التسجيل# (فى حالة عدم وجود رقم تسجيل,ادخلUR قبل 3 ارقام ',
    'contractingauthority' => 'جهة التعاقد',

    'union' => 'نقابات',
    'company' => 'شركات',
    'personal' => 'ضخصى',
    'protocol' => 'بروتوكول',


    'enrollmentperiod' => 'دفعة الإلتحاق بالبرنامج',
    'October' => 'اكتوبر',
    'February' => 'فبراير',

    'enrollmentyear' => 'السنة',
    'studycity' => 'المحافظة',

    'dicountstatus' => 'حالة الخصم',
    'pending' => 'قيد التفعيل',
    'active' => 'مفعل',
    'inactive' => 'غير مفعل',
    'percentageofdiscount' => 'نسبة الخصم',


    'academicstatus' => 'الحالة الدراسية',

    'courses' => 'قائمة المواد المسجلة',
    'progtype' => ' نوع البرنامج',
    'attend' => 'الحضور',
    'certificates' => 'الشهادات',
    'complains' => 'الشكاوى او الطلبات',
    'comments' => 'ملاحظات او تعليقات',
    'none' => 'لايوجد',
    'paymentfulfillment' => 'السداد',
    'initial' => 'مقدم',
    'hourly' => 'ساعات',
    'numofhours' => 'عدد الساعات',
    'groupname/num' => 'جهة التعاقد',
    'masters' => 'Masters',
    'pre-masters' => 'Pre-masters',
    'diploma' => 'Diploma',
    'arabic' => 'عربى',
    'english' => 'انجليزى ',
    'intensive' => 'مكثف',
    'quality' => 'Quality',
    'h&s' => 'H&S',
    'hours' => 'Hours',
    'foodsafety' => 'Food Safety',
    'healthcare' => 'Health Care',
    'file' => 'ملف',
    'upload' => 'اختار ملف او اكثر',
    'Alexandria'=>'Alexandria',
    'Aswan' => 'Aswan',
    'Asyut'=>'Asyut',   
    'Beheira'=>'Beheira',   
    'BeniSuef' =>'Beni Suef',
    'Cairo' => 'Cairo',
    'Dakahlia' =>'Dakahlia',    
    'Damietta' =>'Damietta',
    'Faiyum' =>'Faiyum',    
    'Gharbia' =>'Gharbia',  
    'Giza' =>'Giza',    
    'Ismailia' =>'Ismailia',    
    'KafrElSheikh' =>'Kafr El Sheikh',
    'Luxor' =>'Luxor',
    'Matruh' =>'Matruh',    
    'Minya' =>'Minya',  
    'Monufia' =>'Monufia',
    'NewValley' =>'New Valley', 
    'NorthSina' =>'North Sina',
    'PortSaid' =>'Port Said',   
    'Qalyubia' =>'Qalyubia',    
    'Qena' =>'Qena',    
    'RedSea' =>'Red Sea',   
    'Sharqia' =>'Sharqia',  
    'Sohag' =>'Sohag',  
    'SouthSinai' =>'South Sinai',  
    'Suez' =>'Suez',
];
