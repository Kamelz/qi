@extends('layouts.view')
@section('content')
<div class="section no-pad-bot" id="index-banner">
	<div class="wrapper" style="    margin-left: 8%;
		margin-right: 10%;">
		<div class="row">
			<div class="col s6">
				<img class="materialboxed ppicture" width="650" id="ppicture" src="../../storage/app/{{$student['pphoto']}}"/>

			</div>
			@if(Auth::user()->is_admin)
			<div class="col s6 ">
				<a href="#add_section" style="cursor:pointer; color: white; font-size: 40px;" class="material-icons light-blue lighten-1 ">arrow_downward</a>
			</div>
			@endif
		</div>
		<div class="row">
			@if ($errors->any())
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<div class="card-panel red lighten-3">
                    <span  style="color:white">
                        {{ $error }}
                    </span>
						</div>
					@endforeach
				</div>
			@endif
			@if (session('status'))
				<div class="success_message light-blue lighten-1 alert alert-success">
					{{ session('status') }}
				</div>
			@endif
		</div>
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Student Information</h4></li>
				<li class="collection-item">Student name: <span class="bold" >{{$student['studentname']}}</span></li>
				<li class="collection-item">Nationality: <span class="bold">{{$student['nationality']}}</span></li>
				<li class="collection-item">Reg#:<span class="bold"> {{$student['regnum']}}</span></li>
				<li class="collection-item">GPA:<span class="bold">{{$student['gpa']}}</span></li>
				<li class="collection-item">College Major: <span class="bold">{{$student['collegemajor']}}</span></li>
				<li class="collection-item">College: <span class="bold">{{$student['college']}}</span></li>
				<li class="collection-item">Telephone Number:<span class="bold"> {{$student['telephonenumber']}}</span></li>
				<li class="collection-item">Mobile Number:<span class="bold"> {{$student['mobilenumber']}}</span></li>
				<li class="collection-item">Email: <span class="bold">{{$student['studentemail']}}</span></li>
				<li class="collection-item">Contracting Authority:<span class="bold"> {{$student['contractingauthority']}}</span></li>
				<li class="collection-item">Contracting Authority info: <span class="bold">{{$student['contractingauthorityinfo']}}</span></li>
				<li class="collection-item">Studyt City: <span class="bold">{{$student['studytcity']}}</span></li>
				<li class="collection-item">Enrollment Period:<span class="bold"> {{$student['enrollmentperiod']}}</span></li>
				<li class="collection-item">Enrollment Year: <span class="bold">{{$student['enrollmentyear']}}</span></li>
				<li class="collection-item">Dicount Status:<span class="bold"> {{$student['dicountstatus']}}</span></li>
				<li class="collection-item">Percentage Of discount:<span class="bold"> {{$student['percentageofdiscount']}}</span></li>
				<li class="collection-item">Academic Status: <span class="bold">{{$student['academicstatus']}}</span></li>
				<li class="collection-item">Group Coordinator: <span class="bold">{{$student['groupcoordinator']}}</span></li>
			</ul>
		</div>
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Student Payment</h4></li>
				@if(count($studentPayments))

				@foreach($studentPayments as $data)
					<li class="collection-item">Payment fulfillment: <span class="bold">{{$data->paymentfulfillment }}</span></li>
					<li class="collection-item">Enrollment year: <span class="bold">{{$data->enrollmentyear}}</span></li>
					<li class="collection-item">Enrollment period: <span class="bold">{{$data->enrollmentyear}}</span></li>
					@if(isset($data->numberofhours))
						<li class="collection-item">Number of hours: <span class="bold">{{$data->numberofhours}}</span></li>
					@endif
						<br>
				@endforeach
				
				@else
				<li class="collection-item">no information available</li>
				@endif
			</ul>
		</div>
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Student Certificates</h4></li>
				@if(count($studentCertificates))
				@foreach($studentCertificates as $file)
				<li class="collection-item"><a href="../../storage/app/{{$file->certimage}}">{{ str_replace("certificates/",'',$file->certimage)}}</a></li>
				@endforeach

				@else
				<li class="collection-item">no information available</li>
				@endif
			</ul>
		</div>

		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Academic files</h4></li>
				@if(count($academicFiles))
				@foreach($academicFiles as $file)
				<li class="collection-item"><a href="../../storage/app/{{$file->path}}">{{ str_replace("academicfiles/",'',$file->path)}}</a></li>
				@endforeach
				@else
				<li class="collection-item">no information available</li>
				@endif
			</ul>
		</div>
		
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Extra information</h4></li>
				@if(count($studentInfo))
				@foreach($studentInfo as $info)
				
				@if(count($studentInfo))
				
				<li class="collection-item">Comment: <span class="bold"> {{ $info->comment}} </span></li>
				@else
				<li class="collection-item">no comments</li>
				@endif
				@if(count($studentInfo))
				
				<li class="collection-item">complaint: <span class="bold"> {{ $info->complainttext}} </span></li>
				@else
				<li class="collection-item">no complain</li>
				@endif
				<br>
				@endforeach
				@else
				<li class="collection-item">no information available</li>
				@endif
			</ul>
		</div>
		
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Attended Courses</h4></li>
				@foreach($studentCourses as $course)
				
				<li class="collection-item">
					Code: <span class="bold">{{$course->coursecode}}</span>
				</li>
				<li class="collection-item">
					Name: <span class="bold">{{$course->coursename}}</span>
				</li>
				
				<li class="collection-item">
					Credit hours: <span class="bold">{{$course->credithours}}</span>
				</li>
				<li class="collection-item">
					Enrollment period: <span class="bold">{{$course->enrollmentperiod}}</span>
				</li>
				<li class="collection-item">
					Enrollment year: <span class="bold">{{$course->enrollmentyear}}</span>
				</li>
				
				<br>				
				@endforeach
			</ul>
		</div>
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Registrated Courses</h4></li>
				@foreach($studentRegCourses as $course)
				
				<li class="collection-item">
					Code: <span class="bold">{{$course->coursecode}}</span>
				</li>
				<li class="collection-item">
					Name: <span class="bold">{{$course->coursename}}</span>
				</li>
				
				<li class="collection-item">
					Credit hours: <span class="bold">{{$course->credithours}}</span>
				</li>
				<li class="collection-item">
					Enrollment period: <span class="bold">{{$course->enrollmentperiod}}</span>
				</li>
				<li class="collection-item">
					Enrollment year:<span class="bold"> {{$course->enrollmentyear}}</span>
				</li>
				<br>
				@endforeach
			</ul>
		</div>
		<div class="row">
			<ul class="collection with-header">
				<li class="collection-header"><h4>Student Activity</h4></li>
				@if(count($studentActivites))
				@foreach($studentActivites as $activity)
				
				<li class="collection-item">
					Type: <span class="bold">{{$activity->eatype}}</span>
				</li>
				<li class="collection-item">
					Info: <span class="bold">{{$activity->extraactivityinfo}}</span>
				</li>
					<br>
				
				@endforeach
				@else
				<li class="collection-item">no information available</li>
				@endif
			</ul>
		</div>
		@if(Auth::user()->is_admin)
		<div id="add_section" class="scrollspy row">
			<h4>Attend course</h4>
			<form id="studentCoursesForm" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/add/reg/courses/'.$student['id'])}}>

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="row coursesreg-select">
			<div class="coursesreg-row">
				<div class="input-field col s3">
					<select  class="coursesreg validate" name="coursesreg[]">
						<option value="" disabled selected>{{trans('home.coursesreg')}}</option>
						@foreach($courses as $cours)
							<option value="{{$cours->coursecode}}">{{$cours->coursename}}</option>
						@endforeach
					</select>
				</div>
				<div class="input-field col s3">
					<select class="validate" name="daycoursesreg[]">
						<option value="" disabled selected>{{trans('home.day')}}</option>
						<option value="saturday" selected>{{trans('home.saturday')}}</option>
						<option value="sunday" selected>{{trans('home.sunday')}}</option>
						<option value="monday" selected>{{trans('home.monday')}}</option>
						<option value="tuesday" selected>{{trans('home.tuesday')}}</option>
						<option value="wednesday" selected>{{trans('home.wednesday')}}</option>
						<option value="thursday" selected>{{trans('home.thursday')}}</option>
						<option value="friday" selected>{{trans('home.friday')}}</option>
					</select>
				</div>
				<div class="input-field col s3">
					<select class="validate" name="regcourse_enrollmentperiod[]">
						<option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
						<option value="october">{{trans('home.October')}}</option>
						<option value="february">{{trans('home.February')}}</option>
					</select>
					<label>{{trans('home.enrollmentperiod')}}</label>
				</div>
				<div class="input-field col s3">
					<input placeholder="{{trans('home.enrollmentyear')}}"
						   name="regcourse_enrollmentyear[]" id="enrollmentyear" type="number" class="validate">
					<label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
				</div>
			</div>
			<div class="btn green" id="addRegDays">+</div>
		</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder={{trans('home.submit')}}  type="submit"
						   class="btn orange  validate">
				</div>
			</div>
		</form>
		</div>

		<div class="row">

			<h4>Register course</h4>
			<form id="studentAttedndCoursesForm" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/add/attend/courses/'.$student['id'])}}>

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row coursesattend-select">
					<div class="coursesattend-row">
						<div class="input-field col s3">
							<select  name="coursesattend[]">
								<option value="" disabled selected>{{trans('home.coursesattend')}}</option>
								@foreach($courses as $cours)
									<option value="{{$cours->coursecode}}">{{$cours->coursename}}</option>
								@endforeach
							</select>
						</div>
						<div class="input-field col s3">
							<select name="daycoursesattend[]">
								<option value="" disabled selected>{{trans('home.day')}}</option>
								<option value="saturday" selected>{{trans('home.saturday')}}</option>
								<option value="sunday" selected>{{trans('home.sunday')}}</option>
								<option value="monday" selected>{{trans('home.monday')}}</option>
								<option value="tuesday" selected>{{trans('home.tuesday')}}</option>
								<option value="wednesday" selected>{{trans('home.wednesday')}}</option>
								<option value="thursday" selected>{{trans('home.thursday')}}</option>
								<option value="friday" selected>{{trans('home.friday')}}</option>
							</select>
						</div>
						<div class="input-field col s3">
							<select class="validate" name="attendedcourse_enrollmentperiod[]">
								<option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
								<option value="october">{{trans('home.October')}}</option>
								<option value="february">{{trans('home.February')}}</option>
							</select>
							<label>{{trans('home.enrollmentperiod')}}</label>
						</div>
						<div class="input-field col s3">
							<input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
								   name="attendedcourse_enrollmentyear[]" id="enrollmentyear" type="number" class="validate">
							<label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
						</div>

					</div>
					<div class="row">
						<div class="col s4">
							<span id="addAttendRegDays" class="btn green">+</span>
						</div>
					</div>

				</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder={{trans('home.submit')}}  type="submit"
						   class="btn orange  validate">
				</div>
			</div>
		</form>
		</div>

		<form id="studentAttedndCoursesForm" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/add/payment/'.$student['id'])}}>
			<h4>Add Payment</h4>
		<div class="row">
			<div class="row paymentmethod-row">
				<div  class=" row payment-form ">
					<div class="input-field col s3">
						<select id="paymentfulfillment" onchange="checkPaymetnType(this)" class="paymentfulfillment required" name="paymentfulfillment[]">
							<option value="" selected disabled >{{trans('home.paymentfulfillment')}}</option>
							<option value="initial">{{trans('home.initial')}}</option>
							<option  value="hourly">{{trans('home.hourly')}}</option>
						</select>
						<label>{{trans('home.paymentfulfillment')}}</label>
					</div>
					<div class="input-field col s3">
						<select  id="numofhours" name="numofhours[]">
							<option value="0" selected>{{trans('home.numofhours')}}</option>
							<option value="3">3</option>
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select>
						<label>{{trans('home.numofhours')}}</label>
					</div>

					<div class="input-field col s3">
						<select class="validate" name="payment_enrollmentperiod[]">
							<option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
							<option value="october">{{trans('home.October')}}</option>
							<option value="february">{{trans('home.February')}}</option>
						</select>
						<label>{{trans('home.enrollmentperiod')}}</label>
					</div>
					<div class="input-field col s3">
						<input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
							   name="payment_enrollmentyear[]" id="payment_enrollmentyear" type="text" class="validate">
						<label for="payment_enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
					</div>

				</div>

				<div class="row">
					<div class="col s4">
						<span id="append-payment-fullfillment" class="btn green">+</span>
					</div>
				</div>
				</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

		</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder={{trans('home.submit')}}  type="submit"
						   class="btn orange  validate">
				</div>
			</div>
		</form>
		@endif
	</div>
</div>
@endsection