@extends('layouts.view')

@section('content')

    <link rel="stylesheet" type="text/css" href={{asset('css/material.min.css')}}>

<div class="section no-pad-bot" id="index-banner">
    <div class="wrapper">
     <div class="row student_form">

         <div class="row">
                <div class="col s12">
                    <h5>Student Search</h5>
                </div>
            </div>
        </div>
        {{--  <table id="user-posts-table"  class="table table-bordered">  --}}
        <table id="user-posts-table"  class="mdl-data-table">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Reg#</th>
                <th>Name</th>
                <th>GPA</th>
                <th>Nationality</th>
                <th>Major</th>
                <th>Telephone number</th>
                <th>Mobile number</th>
                <th>College</th>
                <th>Student email</th>
                <th>Group coordinator</th>
                <th>Contracting authority</th>
                <th>Contracting authority info</th>
                <th>City</th>
                <th>Enrollment period</th>
                <th>Enrollment year</th>
                <th>Dicount status</th>
                <th>Percentage of discount</th>
                <th>Academic status</th>
                <th>Courses</th>
                <th>Extra activity</th>
                <th>Info</th>
            </tr>
            </thead>
        </table>
        <div class="row student_form">
            <div class="row">
                    <div class="col s12">
                        <h5>Instructor Search</h5>
                    </div>
                </div>
            </div>
        </div>


        <table id="instructor-posts-table"  class="table table-bordered">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Name</th>
                <th>Rank</th>
                <th>position</th>
                <th>number</th>
                <th>CV</th>
                <th>Courses</th>
            </tr>
            </thead>
        </table>
              <div class="row student_form">
            <div class="row">
                    <div class="col s12">
                        <h5>Courses Search</h5>
                    </div>
                </div>
            </div>
        <table id="courses-posts-table"  class="table table-bordered">
            <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Credit hours</th>
                <th>Instructors</th>
                <th>Attending students</th>
                <th>Registered students</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

@endsection

