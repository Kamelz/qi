@extends('layouts.view')
@section('content')
<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4>{{trans('home.addactivity')}}</h4>
        {{--<input  type="text">--}}
        <div id="activity" class="chips"></div>
    </div>
    <div class="modal-footer">
        <a href="#!" id="submitactivity"
        class="modal-action modal-close waves-effect waves-green btn-flat">{{trans('home.submit')}}</a>
    </div>
</div>
<div class="section no-pad-bot" id="index-banner">
    <div class="wrapper">
        <div class="row student_form">
            @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <div class="card-panel red lighten-3">
                    <span  style="color:white">
                        {{ $error }}
                    </span>
                </div>
                @endforeach
            </div>
            @endif
            @if (session('status'))
            <div class="success_message light-blue lighten-1 alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
                <div class="col s12">
                    <h5>Add a new Student</h5>
                </div>
            </div>
            <form id="studentform" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/student')}}>
                <div class="row">
                    <div class="file-field  input-field col s6">
                        <div class="btn orange">
                            <span>{{trans('home.ppicture')}}</span>
                            <input name="picture" type="file" id="imgInp">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <img class="materialboxed ppicture" width="650" id="ppicture" src= {{asset('images/if_unknown_403017.png')}} />
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.name')}} id="name" value="{{old('name')}}" name="name"
                        type="text" class="validate">
                        <label for="name"> {{trans('home.name')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.nationality')}} name="nationality"
                        value="{{old('nationality')}}" id="nationality" type="text" class="validate">
                        <label for="nationality"> {{trans('home.nationality')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.major')}} id="major" value="{{old('major')}}" name="major"
                        type="text" class="validate">
                        <label for="major"> {{trans('home.major')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.gpa')}} id="gpa" value="{{old('gpa')}}" name="gpa" type="text"
                        class="validate">
                        <label for="gpa"> {{trans('home.gpa')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.college')}} id="college" value="{{old('college')}}"
                        name="college" type="text" class="validate">
                        <label for="college"> {{trans('home.college')}}</label>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.telephone')}} id="telephone" value="{{old('telephone')}}"
                        name="telephone" type="number" class="validate">
                        <label for="telephone"> {{trans('home.telephone')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.mobile')}} id="mobile" value="{{old('mobile')}}" name="mobile"
                        type="number" class="validate">
                        <label for="mobile"> {{trans('home.mobile')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.email')}} id="email" value="{{old('email')}}" name="email"
                        type="email" class="validate">
                        <label for="email"> {{trans('home.email')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.regn')}} id="regn" name="regn" value="{{old('regn')}}"
                        type="text" class="validate">
                        <label for="regn"> {{trans('home.regn')}}</label>
                    </div>
                </div>
                <div class="input-field col s6">
                    <select class="validate" name="enrollmentperiod">
                        <option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
                        <option value="october">{{trans('home.October')}}</option>
                        <option value="february">{{trans('home.February')}}</option>
                    </select>
                    <label>{{trans('home.enrollmentperiod')}}</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
                    name="enrollmentyear" id="enrollmentyear" type="text" class="validate">
                    <label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
                </div>
                <div class="row contractingauthority">
                    <div class="input-field col s6">
                        <select id="contractingauthority" name="contractingauthority">
                            <option value="" disabled selected>{{trans('home.contractingauthority')}}</option>
                            <option value="union">{{trans('home.union')}}</option>
                            <option value="company">{{trans('home.company')}}</option>
                            <option value="personal">{{trans('home.personal')}}</option>
                            <option value="protocol">{{trans('home.protocol')}}</option>
                        </select>
                        <label>{{trans('home.contractingauthority')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="row">
                        <div class="col s12">
                            <label>Program type</label>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($tracktypes as $type)
                        <div class="row">
                            <div class="col s6">
                                <input onclick="getTracks({{$type->id}})" class="with-gap" value="{{$type->id}}"
                                name="progtype" type="radio" id="{{$type->tracktype}}"/>
                                <label for="{{$type->tracktype}}">{{$type->tracktype}}</label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div id="tracks" class="input-field col s6">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <select name="studycity">
                            <option value="" disabled selected>{{trans('home.studycity')}}</option>
                            <option value={{trans('home.Alexandria')}}>{{trans('home.Alexandria')}}</option>
                            <option value={{trans('home.Aswan')}}>{{trans('home.Aswan')}}</option>
                            <option value={{trans('home.Asyut')}}>{{trans('home.Asyut')}}</option>
                            <option value={{trans('home.Beheira')}}>{{trans('home.Beheira')}}</option>
                            <option value={{trans('home.BeniSuef')}}>{{trans('home.BeniSuef')}}</option>
                            <option value={{trans('home.Cairo')}}>{{trans('home.Cairo')}}</option>
                            <option value={{trans('home.Dakahlia')}}>{{trans('home.Dakahlia')}}</option>
                            <option value={{trans('home.Damietta')}}>{{trans('home.Damietta')}}</option>
                            <option value={{trans('home.Faiyum')}}>{{trans('home.Faiyum')}}</option>
                            <option value={{trans('home.Gharbia')}}>{{trans('home.Gharbia')}}</option>
                            <option value={{trans('home.Giza')}}>{{trans('home.Giza')}}</option>
                            <option value={{trans('home.Ismailia')}}>{{trans('home.Ismailia')}}</option>
                            <option value={{trans('home.KafrElSheikh')}}>{{trans('home.KafrElSheikh')}}</option>
                            <option value={{trans('home.Luxor')}}>{{trans('home.Luxor')}}</option>
                            <option value={{trans('home.Matruh')}}>{{trans('home.Matruh')}}</option>
                            <option value={{trans('home.Minya')}}>{{trans('home.Minya')}}</option>
                            <option value={{trans('home.Monufia')}}>{{trans('home.Monufia')}}</option>
                            <option value={{trans('home.NewValley')}}>{{trans('home.NewValley')}}</option>
                            <option value={{trans('home.NorthSina')}}>{{trans('home.NorthSina')}}</option>
                            <option value={{trans('home.PortSaid')}}>{{trans('home.PortSaid')}}</option>
                            <option value={{trans('home.Qalyubia')}}>{{trans('home.Qalyubia')}}</option>
                            <option value={{trans('home.Qena')}}>{{trans('home.Qena')}}</option>
                            <option value={{trans('home.RedSea')}}>{{trans('home.RedSea')}}</option>
                            <option value={{trans('home.Sharqia')}}>{{trans('home.Sharqia')}}</option>
                            <option value={{trans('home.Sohag')}}>{{trans('home.Sohag')}}</option>
                            <option value={{trans('home.SouthSinai')}}>{{trans('home.SouthSinai')}}</option>
                            <option value={{trans('home.Suez')}}>{{trans('home.Suez')}}</option>
                        </select>
                        <label>{{trans('home.studycity')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <select name="trackgroup">
                            <option value="" disabled selected>{{trans('home.group')}}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                        <label>{{trans('home.group')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <select  class="instructor" name="instructor">
                            <option value="" disabled selected>{{trans('home.instructor')}}</option>
                            @foreach($instructors as $instructor)
                            <option value="{{$instructor->instructorname}}">{{$instructor->instructorname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row coursesreg-select">
                    <div class="coursesreg-row">
                        <div class="input-field col s3">
                            <select  class="coursesreg validate" name="coursesreg[]">
                                <option value="" disabled selected>{{trans('home.coursesreg')}}</option>
                                @foreach($courses as $cours)
                                <option value="{{$cours->coursecode}}">{{$cours->coursename}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-field col s3">
                            <select class="validate" name="daycoursesreg[]">
                                <option value="" disabled selected>{{trans('home.day')}}</option>
                                <option value="saturday" selected>{{trans('home.saturday')}}</option>
                                <option value="sunday" selected>{{trans('home.sunday')}}</option>
                                <option value="monday" selected>{{trans('home.monday')}}</option>
                                <option value="tuesday" selected>{{trans('home.tuesday')}}</option>
                                <option value="wednesday" selected>{{trans('home.wednesday')}}</option>
                                <option value="thursday" selected>{{trans('home.thursday')}}</option>
                                <option value="friday" selected>{{trans('home.friday')}}</option>
                            </select>
                        </div>
                          <div class="input-field col s3">
                            <select class="validate" name="regcourse_enrollmentperiod[]">
                                <option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
                                <option value="october">{{trans('home.October')}}</option>
                                <option value="february">{{trans('home.February')}}</option>
                            </select>
                            <label>{{trans('home.enrollmentperiod')}}</label>
                        </div>
                        <div class="input-field col s3">
                            <input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
                            name="regcourse_enrollmentyear[]" id="enrollmentyear" type="number" class="validate">
                            <label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
                        </div>
                    </div>
                    <div class="btn green" id="addRegDays">+</div>
                </div>
                <div class="row coursesattend-select">
                    <div class="coursesattend-row">
                        <div class="input-field col s3">
                            <select  name="coursesattend[]">
                                <option value="" disabled selected>{{trans('home.coursesattend')}}</option>
                                @foreach($courses as $cours)
                                <option value="{{$cours->coursecode}}">{{$cours->coursename}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-field col s3">
                            <select name="daycoursesattend[]">
                                <option value="" disabled selected>{{trans('home.day')}}</option>
                                <option value="saturday" selected>{{trans('home.saturday')}}</option>
                                <option value="sunday" selected>{{trans('home.sunday')}}</option>
                                <option value="monday" selected>{{trans('home.monday')}}</option>
                                <option value="tuesday" selected>{{trans('home.tuesday')}}</option>
                                <option value="wednesday" selected>{{trans('home.wednesday')}}</option>
                                <option value="thursday" selected>{{trans('home.thursday')}}</option>
                                <option value="friday" selected>{{trans('home.friday')}}</option>
                            </select>
                        </div>
                        <div class="input-field col s3">
                            <select class="validate" name="attendedcourse_enrollmentperiod[]">
                                <option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
                                <option value="october">{{trans('home.October')}}</option>
                                <option value="february">{{trans('home.February')}}</option>
                            </select>
                            <label>{{trans('home.enrollmentperiod')}}</label>
                        </div>
                        <div class="input-field col s3">
                            <input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
                            name="attendedcourse_enrollmentyear[]" id="enrollmentyear" type="number" class="validate">
                            <label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
                        </div>
                        
                    </div>
                    <div class="btn green" id="addAttendRegDays">+</div>
                </div>

                <div class="row">
                    <div class="input-field col s4">
                        <select name="dicountstatus">
                            <option value="" disabled selected>{{trans('home.dicountstatus')}}</option>
                            <option value="pending">{{trans('home.pending')}}</option>
                            <option value="active">{{trans('home.active')}}</option>
                            <option value="inactive">{{trans('home.inactive')}}</option>
                            <option value="none">{{trans('home.none')}}</option>
                        </select>
                        <label>{{trans('home.dicountstatus')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder={{trans('home.percentageofdiscount')}}
                        value="{{old('percentageofdiscount')}}" name="percentageofdiscount"
                        id="percentageofdiscount" type="number" class="validate">
                        <label for="percentageofdiscount"> {{trans('home.percentageofdiscount')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <select id="academicstatus" name="academicstatus">
                            <option value="" disabled selected>{{trans('home.academicstatus')}}</option>
                            <option value="graduated">{{trans('home.graduated')}}</option>
                            <option value="withdrawal">{{trans('home.withdrawal')}}</option>
                            <option value="postponement">{{trans('home.postponement')}}</option>
                            <option value="regular">{{trans('home.regular')}}</option>
                            <option value="conversion">{{trans('home.Transfer')}}</option>
                            <option value="cancelled registration">{{trans('home.CancelledRegistration')}}</option>
                        </select>
                        <label>{{trans('home.academicstatus')}}</label>
                    </div>
                    <div id="academicfiles" class="file-field hide input-field col s4">
                        <div class="btn orange">
                            <span>Aacademic files</span>
                            <input name="academicfiles[]" type="file" multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder={{trans('home.upload')}}>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="row extraactivity-row">
                        <div  class=" row extra-activity-form ">
                            <div  class="input-field col s6">
                                <select class="extraactivity-select" onchange="checkIfOtherSelected(this)" name="extraactivity[]">
                                    <option value="" disabled selected>{{trans('home.extraactivity')}}</option>
                                    @foreach($extraactivity as $activity)
                                    <option value={{$activity->id}}>{{$activity->eatype}}</option>
                                    @endforeach
                                
                                </select>
                            </div>
                            <div style="display:none" class="extra-activity-info input-field col s6">
                                <input name="extraactivityinfo[]" placeholder= {{trans('home.extraactivityinfo')}}  type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4">
                            <span id="append-activity" class="btn green">+</span>
                        </div>
                    </div>
                </div>
                {{--<div class="row otheactiviy">--}}
                    {{--<div class="input-field col s6">--}}
                        {{--<div type="text"  id="otheactiviy" class="chips tooltipped" data-position="bottom" data-delay="50" data-tooltip="Press Enter when you finish typing."  ></div>--}}
                        {{--<label>{{trans('home.other')}}</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="row">
                    <div class="row paymentmethod-row">
                        <div  class=" row payment-form ">
                            <div class="input-field col s3">
                                <select id="paymentfulfillment" onchange="checkPaymetnType(this)" class="paymentfulfillment required" name="paymentfulfillment[]">
                                    <option value="" selected disabled >{{trans('home.paymentfulfillment')}}</option>
                                    <option value="initial">{{trans('home.initial')}}</option>
                                    <option  value="hourly">{{trans('home.hourly')}}</option>
                                </select>
                                <label>{{trans('home.paymentfulfillment')}}</label>
                            </div>
                            <div class="input-field col s3">
                                <select  id="numofhours" name="numofhours[]">
                                    <option value="0" selected>{{trans('home.numofhours')}}</option>
                                    <option value="3">3</option>
                                    <option value="6">6</option>
                                    <option value="9">9</option>
                                    <option value="12">12</option>
                                </select>
                                <label>{{trans('home.numofhours')}}</label>
                            </div>

                            <div class="input-field col s3">
                                <select class="validate" name="payment_enrollmentperiod[]">
                                    <option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
                                    <option value="october">{{trans('home.October')}}</option>
                                    <option value="february">{{trans('home.February')}}</option>
                                </select>
                                <label>{{trans('home.enrollmentperiod')}}</label>
                            </div>
                            <div class="input-field col s3">
                                <input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
                                       name="payment_enrollmentyear[]" id="payment_enrollmentyear" type="text" class="validate">
                                <label for="payment_enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col s4">
                            <span id="append-payment-fullfillment" class="btn green">+</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label>Upload certificates </label>
                    <div class="file-field  input-field col s6">
                        <div class="btn orange">
                            <span>Upload certificates</span>
                            <input name="certificates[]" type="file" multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder={{trans('home.upload')}}>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea name="complains" value="{{old('complains')}}" id="complains"
                        class="materialize-textarea" length="140"></textarea>
                        <label for="complains">{{trans('home.complains')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea name="comments" value="{{old('comments')}}" id="comments"
                        class="materialize-textarea" length="140"></textarea>
                        <label for="comments">{{trans('home.comments')}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder={{trans('home.submit')}} id="submit" type="submit"
                        class="btn orange  validate">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection