@extends('layouts.view')
@section('content')
<div class="section no-pad-bot" id="index-banner">
    <div class="wrapper">
        @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <div class="card-panel red lighten-3">
                <span  style="color:white">
                    {{ $error }}
                </span>
            </div>
            @endforeach
        </div>
        @endif
        @if (session('status'))
        <div class="success_message light-blue lighten-1 alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="row instructor_form">
            <div class="row">
                <div class="col s12">
                    <h5>Add a new Instructor</h5>
                </div>
            </div>
            <form id="instructoraddform" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/instructor/add')}}>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="file-field  input-field col s6">
                        <div class="btn orange">
                            <span>{{trans('home.ppicture')}}</span>
                            <input name="picture" type="file" id="imgInp">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <img class="materialboxed ppicture" width="650" id="ppicture" src= {{asset('images/if_unknown_403017.png')}} />
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" id="instructor" name="instructor"/>
                        <label for="instructor"> Instructor</label>
                    </div>
                </div>
                <div class="row coursesreg-select">
                    <div class="coursesreg-row">
                        <div class="input-field col s6">
                            <input type="text" name="rank" id="rank">
                            <label for="rank"> Rank</label>
                        </div>
                    </div>
                </div>
                <div class="row coursesreg-select">
                    <div class="coursesreg-row">
                        <div class="input-field col s6">
                            <input type="text" name="position" id="position">
                            <label for="position"> Position</label>
                        </div>
                    </div>
                </div>
                <div class="row coursesreg-select">
                    <div class="coursesreg-row">
                        <div class="input-field col s6">
                            <input type="number" name="number" id="number">
                            <label for="number"> Employee number</label>
                        </div>
                    </div>
                </div>
                <div class="row coursesreg-select">
                    <div class="coursesreg-row">
                        <label for="cv"> CV</label>
                        <div class="file-field input-field col s6">
                            <div class="btn orange">
                                <span>CV</span>
                                <input name="cv" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload CV">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder={{trans('home.submit')}} id="submit" type="submit"
                        class="btn orange  validate">
                    </div>
                </div>
            </form>
        </div>
        <div class="row instructor_form">
            <div class="row">
                <div class="col s12">
                    <h5>Assign courses to an instructor </h5>
                </div>
            </div>
            <form id="instructorform" class="col s12" enctype="multipart/form-data" method="POST" action={{url('/instructor')}}>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s6">
                        <select  class="instructor" name="instructor">
                            <option value="" disabled selected>Instructors</option>
                            @foreach($instructors as $instructor)
                            <option value={{$instructor->id}}>{{$instructor->instructorname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <select  name="group">
                            <option value="" disabled selected>Groups</option>
                            @foreach($groups as $group)
                            <option value="{{$group->id}}">
                                {{$group->name .'/'.$group->tracktype.'/'.$group->trackname}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row coursesattend-select">
                    <div class="coursesattend-row">
                        <div class="input-field col s3">
                            <select  name="coursesattend[]">
                                <option value="" disabled selected>Courses</option>
                                @foreach($courses as $cours)
                                <option value="{{$cours->coursecode}}">{{$cours->coursename}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-field col s3">
                            <select name="day[]">
                                <option value="" disabled selected>{{trans('home.day')}}</option>
                                <option value="saturday" selected>{{trans('home.saturday')}}</option>
                                <option value="sunday" selected>{{trans('home.sunday')}}</option>
                                <option value="monday" selected>{{trans('home.monday')}}</option>
                                <option value="tuesday" selected>{{trans('home.tuesday')}}</option>
                                <option value="wednesday" selected>{{trans('home.wednesday')}}</option>
                                <option value="thursday" selected>{{trans('home.thursday')}}</option>
                                <option value="friday" selected>{{trans('home.friday')}}</option>
                            </select>
                        </div>
                        <div class="input-field col s3">
                            <select class="validate" name="enrollmentperiod[]">
                                <option value="" disabled selected>{{trans('home.enrollmentperiod')}}</option>
                                <option value="october">{{trans('home.October')}}</option>
                                <option value="february">{{trans('home.February')}}</option>
                            </select>
                            <label>{{trans('home.enrollmentperiod')}}</label>
                        </div>
                        <div class="input-field col s3">
                            <input placeholder={{trans('home.enrollmentyear')}} value="{{old('enrollmentyear')}}"
                            name="enrollmentyear[]" id="enrollmentyear" type="number" class="validate">
                            <label for="enrollmentyear"> {{trans('home.enrollmentyear')}}</label>
                        </div>
                        
                    </div>
                    <div class="btn green" id="addAttendRegDays">+</div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder={{trans('home.submit')}} id="submit" type="submit"
                        class="btn orange  validate">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection