

<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AAST</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <link rel="stylesheet" type="text/css" href={{asset('temp/css/materialize.min.css')}}>

    <link rel="stylesheet" type="text/css" href={{asset('css/dataTables.material.min.css')}}>

    <link rel="stylesheet" type="text/css" href={{asset('temp/css/jquery.dataTables.min.css')}}>
    <link rel="stylesheet" type="text/css" href={{asset('temp/css/style.css')}}>


</head>
<body>



<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper">
        <a href={{url('/')}} class="brand-logo">{{trans('home.brand')}}</a>
        <ul id="nav-web" class="right hide-on-med-and-down">

            @if(Auth::check() && Auth::user()->is_admin)
            <li><a href={{url('/')}}>New Student</a></li>
            <li><a href={{url('/instructor')}}>Instructor/Courses</a></li>

            @endif
            @if(Auth::check())
                    <li><a href={{url('/student')}}>Search</a></li>
            @endif
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                       <span style="font-weight: bold">{{ Auth::user()->name }}</span>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                        </li>

                        @endguest
           
        </ul>
    </div>
</nav>


@yield('content')
<script type="text/javascript" src={{asset('temp/js/jquery.min.js')}}></script>
<script type="text/javascript" src={{asset('temp/js/jquery.dataTables.min.js')}}></script>  

<script type="text/javascript" src={{asset('temp/js/materialize.min.js')}}></script>
<script type="text/javascript" src={{asset('js/dataTables.material.min.js')}}></script>
<script type="text/javascript" src={{asset('js/jquery.mousewheel.js')}}></script>
<script type="text/javascript" src={{asset('temp/js/jquery.validate.js')}}></script>
<script type="text/javascript" src={{asset('temp/js/additional-methods.js')}}></script>
<script type="text/javascript" src={{asset('temp/js/script.js')}}></script>

</body>
</html>
